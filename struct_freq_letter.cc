#include <iostream>
#include <vector>
using namespace std;

struct Res {
  char letter;
  double freq;
};

Res most_freq_letter() {
  const int N = int('z') - int('a') + 1;
  vector<int> occs(N, 0);
  int n_letters = 0;
  char c;
  while (cin >> c) {
    if (c >= 'A' and c <= 'Z') c = c - 'A' + 'a';
    if (c >= 'a' and c <= 'z') {
      ++n_letters;
      ++occs[int(c) - int('a')];
    }
  }
  int imax = 0;
  for (int i = 1; i < N; ++i) {
    if (occs[i] > occs[imax]) imax = i;
  }
  Res r;
  r.letter = 'a' + imax;
  if (n_letters > 0) r.freq = double(occs[imax])*100/n_letters;
  else r.freq = 0;
  return r;
}

int main() {
  Res r = most_freq_letter();
  cout << r.letter << ',' << r.freq << '%' << endl;
}
