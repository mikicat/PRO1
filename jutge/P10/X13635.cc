#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

struct Track {
  string artist, title, genre;
  int year;
};

bool comp(const Track &a, const Track &b) {
  if (a.genre == b.genre) {
    if (a.artist == b.artist) {
      if (a.year == b.year) return a.title < b.title;
      return a.year < b.year;
    }
    return a.artist < b.artist;
  }
  return a.genre < b.genre;
}

vector<Track> read_tracks(int n) {
  vector<Track> tracks(n);
  for (int i = 0; i < n; ++i) {
    cin >> tracks[i].artist >> tracks[i].title >> tracks[i].genre >> tracks[i].year;
  }
  return tracks;
}

void print_track(const Track &t) {
  cout << t.artist;
  cout << " (" << t.year;
  cout << ") " << t.title << " (" << t.genre << ")" << endl;
}

int main() {
  int n;
  cin >> n;
  vector<Track> tracks = read_tracks(n);
  string genre;
  sort(tracks.begin(), tracks.end(), comp);
  while (cin >> genre) {
    for (int i = 0; i < tracks.size(); ++i) {
      if (tracks[i].genre == genre) print_track(tracks[i]);
    }
  }
}
