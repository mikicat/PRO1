// A mig
#include <iostream>
#include <vector>
using namespace std;


vector<double> difference(const vector<double>& v1, const vector<double>& v2) {
  int size1 = v1.size();
  int size2 = v2.size();
  vector<double> tmp(size1);
  int i = 0, j = 0;
  int k = 0; // index of tmp vector
  while (i < size1 and j < size2) {
    if (v1[i] < v2[j]) {
      if (k == 0 or v1[i] != tmp[k-1]) {
        tmp[k] = v1[i];
        ++k;
      }
      ++i;
    }
    else if (v1[i] > v2[j]) ++j;
    else ++i;
  }
  while (i < size1) {
    if (k == 0 or v1[i] != tmp[k-1]) {
      tmp[k] = v1[i];
      ++k;
    }
    ++i;
  }
  vector<double> diff(k);
  for (i = 0; i < k; ++i) {
    diff[i] = tmp[i];
    cout << diff[i] << ' ';
  }

  return diff;
}


int main() {
  cout.setf(ios::fixed, ios::floatfield);
  cout.precision(4);
  int n1;
  while (cin >> n1) {
    vector<double> v1(n1);
    for (int i = 0; i < n1; ++i) cin >> v1[i];
    int n2;
    cin >> n2;
    vector<double> v2(n2);
    for (int i = 0; i < n2; ++i) cin >> v2[i];

    vector<double> res = difference(v1, v2);

    int n3 = res.size();
    cout << n3 << endl;
    for (int i = 0; i < n3; ++i) cout << " " << res[i];
    cout << endl << endl;

    for (int r = 0; r < 200; ++r) {
      vector<double> res2 = difference(v1, v2);
      if (res2 != res) cout << "Resultats diferents amb la mateixa entrada!" << endl;
    }
  }
}
