#include <iostream>
using namespace std;

int sum(int n) {
  int next = n;
  while (n > 0) {
    next += n%10;
    n /= 10;
  }
  return next;
}

int encounter_of_rivers (int n) {
  int r1 = 1;
  int r3 = 3;
  int r9 = 9;
  while (n != r1 and n != r3 and n != r9) {
    while (r1 < n) r1 = sum(r1);
    while (r3 < n) r3 = sum(r3);
    while (r9 < n) r9 = sum(r9);
    if (n != r1 and n != r3 and n != r9) n = sum(n);
  }
  return n;
}


int main ()
{
    int n;
    while (cin >> n) {
        cout << n << " " << encounter_of_rivers(n) << endl;
    }
}
