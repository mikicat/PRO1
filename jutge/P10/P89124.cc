#include <iostream>
#include <vector>
using namespace std;

const int MAX_VAL = 1000000;

vector<bool> generate_primes(int n) {
  vector<bool> primes(n+1, true);
  for (int i = 2; i <= n; ++i) {
    if (primes[i]) {
      for (int j = 2*i; j <= n; j += i) {
        primes[j] = false;
      }
    }
  }
  return primes;
}

int main() {
  int n;
  vector<bool> primes;
  primes = generate_primes(MAX_VAL);
  while (cin >> n) {
    if (n == 0 or n == 1) cout << n << " is not prime" << endl;
    else {
      if (primes[n]) cout << n << " is prime";
      else cout << n << " is not prime";
      cout << endl;
    }
  }
}
