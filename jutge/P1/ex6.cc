#include <iostream>
using namespace std;

int main() {
  int t;
  cin >> t;
  bool freeze = t <= 0;
  bool boil = t >= 100;
  if (t < 10) cout << "it's cold" << endl;
  else if (t > 30) cout << "it's hot" << endl;
  else cout << "it's ok" << endl;

  if (boil) cout << "water would boil" << endl;
  if (freeze) cout << "water would freeze" << endl;
}
