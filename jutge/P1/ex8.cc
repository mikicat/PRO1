#include <iostream>
using namespace std;

bool isupper(char c);
int main() {
  char c;
  cin >> c;
  // S'HA DE FER AIXÍ, I NO AMB isupper() NI islower():   if (c >= 'A' and c <= 'Z')
  if (isupper(c)) cout << "uppercase" << endl;
  else cout << "lowercase" << endl;
  c = (char)tolower(c);
  if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') cout << "vowel" << endl;
  else cout << "consonant" << endl;

}
