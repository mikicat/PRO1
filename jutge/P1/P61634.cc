#include <iostream>
using namespace std;

int main() {
  int y;
  cin >> y;
  bool leap = false;

  if (y % 100 == 0)
  {
    if ((y/100) % 4 == 0) leap = true;
  } else {
    if (y % 4 == 0) leap = true;
  }
  if (leap) cout << "YES" << endl;
  else cout << "NO" << endl;
}
