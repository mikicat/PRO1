#include <iostream>
using namespace std;

int main() {
  char f, s;
  cin >> f >> s;
  if (f == s) cout << "-" << endl;
  else if (f == 'A') {
    if (s == 'P') cout << 1 << endl;
    else cout << 2 << endl;
  }
  else if (f == 'P') {
    if (s == 'V') cout << 1 << endl;
    else cout << 2 << endl;
  }
  else if (f == 'V') {
    if (s == 'A') cout << 1 << endl;
    else cout << 2 << endl;
  }
}
