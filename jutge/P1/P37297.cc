#include <iostream>
using namespace std;

int pow (int b, int e);
int main() {
  int n;
  cin >> n;
  int sum = 0;
  for (int i=1; i<= 3; ++i) {
    sum += (n / pow(10, i-1)) - (n / pow(10, i) * 10);
  }

  cout << sum << endl;
}
int pow (int b, int e) {
  int res = 1;
  for (int i=0; i<e; ++i) {
    res *= b;
  }

  return res;
}
