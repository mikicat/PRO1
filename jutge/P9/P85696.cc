#include <iostream>
#include <vector>
using namespace std;

struct Rational {
  int num, den;
};

Rational rational(int n, int d) {
  int n0 = n, d0 = d;
  while (n % d != 0) {
    int dprev = d;
    d = n%d;
    n = dprev;
  }
  Rational r;
  r.num = n0/d, r.den = d0/d;
  if (r.den < 0) r.num *= -1, r.den *= -1;
  return r;
}

int main() {
}
