#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

struct Teacher {
  int carrots, sweets;
  string name;
};

bool comp_teacher(const Teacher& a, const Teacher& b) {
  if (a.sweets == b.sweets) {
    if (a.carrots == b.carrots) {
      if (a.name.length() == b.name.length()) return a.name < b.name;
      else return a.name.length() < b.name.length();
    }
    else return a.carrots > b.carrots;
  }
  else return a.sweets > b.sweets;
}


int main() {
  int n;
  cin >> n;
  for (int i = 0; i < n; ++i) {
    int m;
    cin >> m;
    vector<Teacher> v(m);
    for (int j = 0; j < m; ++j) {
      cin >> v[j].name >> v[j].sweets >> v[j].carrots;
    }

    sort(v.begin(), v.end(), comp_teacher);
    for (int j = 0; j < m; ++j) {
      cout << v[j].name << endl;
    }
    cout << endl;
  }
}
