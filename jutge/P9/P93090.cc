#include <iostream>
using namespace std;

struct Fraction {
    int num, den;
};

int gcd(int n, int d) {
  while (n % d != 0) {
    int dprev = d;
    d = n%d;
    n = dprev;
  }
  return d;
}

Fraction simplify(Fraction old) {
  int n = old.num, d = old.den;
  int mcd = gcd(n, d);
  Fraction r;
  r.num = n/mcd, r.den = d/mcd;
  if (r.den < 0) r.num *= -1, r.den *= -1;
  return r;
}

Fraction addition(const Fraction& x, const Fraction& y) {
    int mcd = gcd(x.den, y.den);
    Fraction res;
    res.den = x.den*y.den/mcd;
    res.num = x.num * (res.den/x.den) + y.num * (res.den/y.den);
    return res;    
}

int main() {
    Fraction x, y;
    char drop;
    cin >> x.num >> drop >> x.den >> drop; 
    x = simplify(x);
    while (drop != '=') {
        cin >> y.num >> drop >> y.den >> drop;
        x = simplify(addition(x, y));
    }
    cout << x.num << '/' << x.den << endl;
    
}
