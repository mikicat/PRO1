#include <iostream>
using namespace std;

struct Time {
  int hour, minute, second;
};

void one_second(const Time& t, Time& t1, Time& t2) {
  t1 = t;
  t1.second += 1;

  if (t1.second >= 60) {
      t1.second -= 60;
      t1.minute += 1;
  }
  if (t1.minute >= 60) {
      t1.minute -= 60;
      t1.hour += 1;
  }
  if (t1.hour >= 24) {
      t1.hour -= 24;
  }

  t2 = t;
  t2.second -= 1;

  if (t2.second < 0) {
      t2.second += 60;
      t2.minute -= 1;
  }
  if (t2.minute < 0) {
      t2.minute += 60;
      t2.hour -=1;
  }
  if (t2.hour < 0) {
      t2.hour += 24;
  }
}

int main() {
  Time time;
  while (cin >> time.hour >> time.minute >> time.second) {
    Time t1, t2;
    one_second(time, t1, t2);
    cout << t1.hour << ' ' << t1.minute << ' ' << t1.second << endl;
    cout << t2.hour << ' ' << t2.minute << ' ' << t2.second << endl;
  }

}
