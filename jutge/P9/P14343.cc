#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

struct Submission {
  string idn;
  string exer;
  int time;
  string res;
};

struct Student {
	string idn;
	int num;
};

struct Results {
	Student send_green;
	Student ex_green;
	Student ex_red;
	Student ex_in;
	Student max_temp;
};

typedef vector<Submission> History;

bool is_shorter(Submission& a, Submission& b) {
  if (a.idn == b.idn) return a.exer < b.exer;
  return a.idn < b.idn;
}

void update(int tried, Student& current, const string& id) {
  if (tried > current.num) {
    current.idn = id;
    current.num = tried;
  }
}

void calc_res(const History& h, Results& r) {
  r.send_green.num = r.ex_green.num = r.ex_red.num = r.ex_in.num = r.max_temp.num = 0;
  int i = 0;
  while (i < h.size()) {
    string stu = h[i].idn;
    int count_ex_green, count_sub_green, count_ex_red, count_ex_in;
    count_ex_green = count_sub_green = count_ex_red = count_ex_in = 0;

    while (h[i].idn == stu and i < h.size()) {
      string ex = h[i].exer;
      bool is_green, is_yellow, is_red;
      is_green = is_yellow = is_red = false;

      while (h[i].exer == ex and h[i].idn == stu and i < h.size()) {
        if (h[i].res == "yellow") is_yellow = true;
        if (h[i].res == "red") is_red = true;
        if (h[i].res == "green") {
          is_green = true;
          ++count_sub_green;
        }
        update(h[i].time, r.max_temp, stu);
        ++i;
      }
      if (is_green) ++count_ex_green;
      if(is_red and not is_green and not is_yellow) ++count_ex_red;
      ++count_ex_in;
    }
    update(count_ex_green, r.ex_green, stu);
    update(count_ex_red, r.ex_red, stu);
    update(count_ex_in, r.ex_in, stu);
    update(count_sub_green, r.send_green, stu);

  }
}


void cout_line(const Student &a, const string &msg, bool has_num) {
	cout << msg;
	if (a.num != 0) {
		cout << a.idn;
		if (has_num) cout << " (" << a.num << ')';
		cout << endl;
	} else cout << '-' << endl;
}

void print_res(const Results &r) {
	cout_line(r.send_green, "student with more green submissions:       ", true);
	cout_line(r.ex_green, "student with more green exercises:         ", true);
	cout_line(r.ex_red, "student with more red exercises:           ", true);
	cout_line(r.ex_in, "student with more tried exercises:         ", true);
	cout_line(r.max_temp, "student who has done the last submission:  ", false);
}

int main() {
	int n;
	cin >> n;
	History h(n);
  for (int i = 0; i < n; ++i) {
    cin >> h[i].idn >> h[i].exer >> h[i].time >> h[i].res;
  }

	sort(h.begin(), h.end(), is_shorter);
	Results r;
	calc_res(h, r);

	print_res(r);
}
