#include <iostream>
#include <vector>
using namespace std;

struct Rational {
  int num, den;
};

int gcd(int n, int d) {
  while (n % d != 0) {
    int dprev = d;
    d = n%d;
    n = dprev;
  }
  return d;
}

Rational simplify(Rational old) {
  int n = old.num, d = old.den;
  int mcd = gcd(n, d);
  Rational r;
  r.num = n/mcd, r.den = d/mcd;
  if (r.den < 0) r.num *= -1, r.den *= -1;
  return r;
}

Rational add(Rational x, Rational y) {
    int mcd = gcd(x.den, y.den);
    Rational res;
    res.den = x.den*y.den/mcd;
    res.num = x.num * (res.den/x.den) + y.num * (res.den/y.den);
    return res;    
}

Rational sub(Rational x, Rational y) {
    int mcd = gcd(x.den, y.den);
    Rational res;
    res.den = x.den*y.den/mcd;
    res.num = x.num * (res.den/x.den) - y.num * (res.den/y.den);
    
    return res;    
}

Rational mult(Rational x, Rational y) {
    Rational res;
    res.num = x.num * y.num;
    res.den = x.den * y.den;
    return res;    
}

Rational div(Rational x, Rational y) {
    Rational res;
    res.num = x.num * y.den;
    res.den = x.den * y.num;
    return res;    
}

int main() {
    Rational x;
    cin >> x.num >> x.den;
    x = simplify(x);
    cout << x.num;
    if (x.den != 1) cout << '/' << x.den;
    cout << endl;
    string op;
    while (cin >> op) {
        Rational y;
        cin >> y.num >> y.den;
        y = simplify(y);
        if (op == "add") x = add(x, y);
        else if (op == "substract") x = sub(x, y);
        else if (op == "multiply") x = mult(x, y);
        else x = div(x, y);
        
        x = simplify(x);
        
        cout << x.num;
        if (x.den != 1) cout << '/' << x.den;
        cout << endl;
    }
    
}
