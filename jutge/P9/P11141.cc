// A mig
#include <iostream>
#include <vector>
using namespace std;

struct Student {
  int idn;
  string name;
  double mark;
  bool repeater;
};

void information(const vector<Student>& stu, double& min, double& max, double& avg) {
  min = -1, max = min, avg = 0;
  int presented = 0;
  bool real_av = false;
  for (int i = 0; i < stu.size(); ++i) {
    double mark = stu[i].mark;
    if (not stu[i].repeater) real_av = true;
    if (mark != -1) {
      if (mark > max) max = mark;

      if (min == -1) min = mark;
      else if (mark < min) min = mark;
      avg += mark;
      ++presented;
    }
  }

  if (avg == 0 and not real_av) avg = -1;
  else avg /= presented;

}

int main() {

}
