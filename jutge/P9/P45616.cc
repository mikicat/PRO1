#include <iostream>
using namespace std;

struct Clock {
  int h, m, s;
};

Clock midnight() {
  Clock reset;
  reset.h = 0, reset.m = 0, reset.s = 0;
  return reset;
}

void increase(Clock& r) {
  ++r.s;
  r.m += r.s/60;
  r.h += r.m/60;
  r.s %= 60;
  r.m %= 60;
  if (r.h >= 24) r.h -= 24;
}

void print(const Clock& r) {
  if (r.h < 10) cout << 0;
  cout << r.h << ':';
  if (r.m < 10) cout << 0;
  cout << r.m << ':';
  if (r.s < 10) cout << 0;
  cout << r.s << endl;
}


int main()
{

}
