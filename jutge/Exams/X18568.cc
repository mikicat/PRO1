#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

typedef vector<char> Fila;
typedef vector<Fila> Matriu;

// Reads matrix n x m from cin
void OmplirMatriu(Matriu& a){
 int n = a.size();
 int m = a[0].size();
 for (int i=0; i < n; ++i){
    for (int j=0; j < m; ++j){
     cin >> a[i][j];
    }
 }
}

// Prints the matrix to cout
void ImprimirMatriu(const Matriu& a){
 int n = a.size();
 int m = a[0].size();
 for (int i=0; i < n; ++i){
    cout << a[i][0];
    for (int j=1; j < m; ++j){
     cout << " " << a[i][j];
    }
    cout << endl;
 }
 cout << endl;
}

// Sort each row of the matrix
void OrdenarCaractersCadaFila(Matriu& a){
  for (int i = 0; i < a.size(); ++i) {
    for (int j = 0; j < a[i].size(); ++j) {
      char min = a[i][j];
      int pos = j;
      for (int k = j; k < a[i].size(); ++k) {
        if (a[i][k] < min) min = a[i][k], pos = k;
      }
      if (pos != j) {
        a[i][pos] = a[i][j];
        a[i][j] = min;
      }
    }
  }
}

// Checks if row a is greater than b (called from SortRows)
bool MesGran(const Fila& a, const Fila& b){
  for (int i = 0; i < a.size(); ++i) {
    if (a[i] != b[i]) return a[i] > b[i];
  }
  return 0; // if all characters are equal, obviously a is not gretaer than b
}

// Swap two rows of the matrix (called from SortRows)
void SwapRows(Fila &a, Fila& b){
  Fila c = a;
  a = b, b = c;
}

// Sort matrix by rows using Bubble sort or Selection sort (your choice)
void SortRows(Matriu& v){
  for (int i = 0; i < v.size(); ++i) {
    Fila min_row = v[i];
    int pos = i;
    for (int j = i; j < v.size(); ++j) {
      if (MesGran(min_row, v[j])) {
        min_row = v[j], pos = j;
      }
    }
    if (pos != i) {
      SwapRows(v[pos], v[i]);
    }
  }
}

int main(){
  int n,m;
  while (cin >> n >> m){
    Matriu a(n, Fila(m));
    OmplirMatriu(a);
    OrdenarCaractersCadaFila(a);
    SortRows(a);
    ImprimirMatriu(a);
   }
}
