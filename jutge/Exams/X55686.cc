#include <iostream>
using namespace std;

int main() {
  int n;
  char c;
  cin >> n >> c;
  if (n % 2 != 0) {
    for (int i = 0; i < n/2; ++i) {
      // Primera meitat
      for (int j = 0; j < i; ++j) cout << '.';
      for (int k = 0; k < 2; ++k) {
        cout << c;
        for (int j = n/2 - 1 - i; j > 0; --j) cout << '.';
      }
      cout << c;
      for (int j = 0; j < i; ++j) cout << '.';
      cout << endl;
    }
    for (int j = 0; j < n; ++j) cout << c; // Linia central
    cout << endl;
    for (int i = n/2 -1; i >= 0; --i) {
      // Segona meitat
      for (int j = 0; j < i; ++j) cout << '.';
      for (int k = 0; k < 2; ++k) {
        cout << c;
        for (int j = n/2 - 1 - i; j > 0; --j) cout << '.';
      }
      cout << c;
      for (int j = 0; j < i; ++j) cout << '.';
      cout << endl;
    }
  }
}
