#include <iostream>
#include <vector>
using namespace std;

typedef vector<char> Row;
typedef vector<Row> Board;

// Reads a board of size nxn from cin, and returns it.
Board read_board(int n) {
   Board b(n, Row(n));
   for (int i = 0; i < n; ++i) {
      for (int j = 0; j < n; ++j) {
          cin >> b[i][j];
      }
   }
   return b;
}

// writes to cout that there is a menace (i1,j1) <--> (i2,j2)
void write_menace(int i1, int j1, int i2, int j2) {
   cout << "(" << i1 << "," << j1 << ") <--> (" << i2 << "," << j2 << ")" <<endl;
}

// searches and writes menaces for cell ib,jb in board b
void find_menaces(const Board & b, int ib, int jb) {
  int n = b[0].size();
  int m = b.size();
  vector< vector<int> > pos(4, vector<int>(2)); // 4 is the max num of bishops in contact
  vector<bool> found(4, false);


  for (int i = 0; i < 4; ++i) {
    int x, y;

    if (i == 0) x = ib - 1, y = jb - 1;
    else if (i == 1) x = ib - 1, y = jb + 1;
    else if (i == 2) x = ib + 1, y = jb - 1;
    else if (i == 3) x = ib + 1, y = jb + 1;

    while ((x < m) and (x >= 0) and (y < n) and (y >= 0) and not found[i]) {
      if (b[x][y] == 'B') {
        pos[i][0] = x, pos[i][1] = y;
        found[i] = true;
      }
      if (i == 0) --x, --y;
      else if (i == 1) --x, ++y;
      else if (i == 2) ++x, --y;
      else if (i == 3) ++x, ++y;
    }
  }

  // Write menace
  for (int i = 0; i < pos.size(); ++i) {
    if (found[i]) write_menace(ib, jb, pos[i][0], pos[i][1]);
  }

}

// searches and writes menaces for all cells in board b
void check_board(const Board& b) {
   Board new_b = b;
   for (int i = 0; i < b.size(); ++i) {
     for (int j = 0; j < b[i].size(); ++j) {
       if (b[i][j] == 'B') find_menaces(new_b, i, j);
       new_b[i][j] = 'o';
     }
   }
}

int main() {
   int n;
   int bn = 1;
   while (cin >> n) {
      Board b = read_board(n);
      cout << "board num " << bn << endl;
      ++bn;
      check_board(b);
      cout << "----------------" << endl;
   }
}
