#include <iostream>
using namespace std;

int abundance(int n);

int main() {
   int x,y;
   while (cin >> x >> y) {
        int ax = abundance(x);
        int ay = abundance(y);
        
        cout << ax << ' ' << ay << ' ';
        if (ax == ay and (ax != 0)) cout << "friends" << endl;
        else cout << "not friends" << endl;
   }
}

int abundance(int n) {
    int index = 0;
    if (n != 1) {
        int sum = 0;
        for (int i = 1; i <= n/2; ++i) {
            if (n % i == 0) sum += i;
        }
        index = sum - n;
    }
    if (index < 0) index = 0;
    return index;
}
