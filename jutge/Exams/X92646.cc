#include <iostream>
#include <vector>
using namespace std;

// reads the  contents of an integer vector of size n  from cin
vector<int> read_vector(int n) {
     vector<int> v(n);
     for (int i = 0; i < n; ++i)
         cin >> v[i];
     return v;
}

void swap(int& x, int& y) {
    int z = x;
    x = y;
    y = z;
}

// reverses a vector in the segment between from and to
void reverse(vector<int>& v, int from, int to) {
    int i = from, j = to - 1;
    while (i < j) {
      v[i] += v[j];
      v[j] = v[i] - v[j];
      v[i] -= v[j];
      ++i, --j;
    }
}


// rotates the vector as explained in the statement of the problem
void rotate_right(vector<int>& v, int k) {
    reverse(v, 0, v.size()); // reverse the whole vector
    reverse(v, 0, k);
    reverse(v, k, v.size());
}

// write a vector of n integers to cout, with blanks to separate
// the n values
void write_vector(const vector<int>& v) {
    for (int i = 0; i < v.size(); ++i) {
      if (i != 0) cout << ' ';
      cout << v[i];
    }
}

int main() {
    int n; cin >> n;
    vector<int> v = read_vector(n);
    int k; cin >> k;
    rotate_right(v, k);
    write_vector(v); cout << endl;
}
