#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

struct Result {
    string city;
    string district;
    int voters;
    int yes;
    int no;
    int blank;
    int null;
};

void read_data(vector<Result>& v) {
    Result r;
    while (cin >> r.city >> r.district >> r.voters >>
                  r.yes >> r.no >> r.blank >> r.null) {
        v.push_back(r);
    }
}

bool compara(const Result &a, const Result &b) {
   return a.district < b.district;
}

// ADD YOUR CODE HERE (procedure compute_and_print, and any other you may need)

void print_results(const Result& r, const float& participation) {
  int votes = r.yes + r.no + r.blank + r.null;
  cout << r.district << ' ' << r.city << ' ' << participation << ' ' << r.voters << ' ' << votes << ' ' << r.voters - votes << ' ' << r.yes << ' ' << r.no << ' ' << r.blank << ' ' << r.null << endl;
}

void compute_and_print(const vector<Result>& v) {
  float max = 0;
  int index = 0;
  for (int i = 0; i < v.size(); ++i) {
    Result r = v[i];
    float participation = (r.yes + r.no + r.blank + r.null)*1.0/r.voters*100; // The *1.0 is put so it can turn int division to float

    if (i != 0) { // If change of district
      if (r.district != v[i - 1].district) {
        print_results(v[index], max);
        index = i, max = participation; // Reset counters
      }
    }

    if (participation > max) max = participation, index = i;
  }
  if (v.size() != 0) print_results(v[index], max); // Prints results of the last district, in case input is not null

}

int main() {
    vector<Result> v;
    read_data(v);
    sort(v.begin(), v.end(), compara);
    compute_and_print(v);
}
