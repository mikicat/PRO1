#include <iostream>
using namespace std;

int main() {
  int x1, x2, x3, x4;
  cin >> x1 >> x2 >> x3 >> x4;
  bool lba = false;

  if ((x1 - x1 / 10) % 2 == 0) {
    if ((x2 - x2 / 10) % 2 != 0 and ((x3 - x3 / 10) % 2 == 0) and ((x4 - x4 / 10) % 2 != 0)) lba = true;
  }
  else {
    if ((x2 - x2 / 10) % 2 == 0 and ((x3 - x3 / 10) % 2 != 0) and ((x4 - x4 / 10) % 2 == 0)) lba = true;
  }

  if (lba) cout << "LBA" << endl;

  if (x1 < x2 and x2 < x3 and x3 < x4) cout << "Increasing" << endl;
  else if (x1 > x2 and x2 > x3 and x3 > x4) cout << "Decreasing" << endl;
  else if (x1 == x3 and x2 == x4) cout << "Folded" << endl;
}
