#include <iostream>
using namespace std;

int pow (int b, int e) {
  int res = 1;
  for (int i = 0; i < e; ++i) res *= b;
  return res;
}

int digit(int n, int pos) {
  return (n/pow(10, pos - 1) - (n/pow(10, pos))*10);
}

int main() {
  int x, y, z;
  cin >> x >> y >> z;
  bool compat = true;
  if (digit(x, 1) != digit(x, 3) or digit(y, 1) != digit(y, 3) or digit(z, 1) != digit(z, 3)) compat = false;
  if (compat) {
    if (digit(x, 1) < digit(y, 1) and digit(y, 1) < digit(z, 1)) cout << "3-Increasing" << endl;
    else if (digit(x, 1) > digit(y, 1) and digit(y, 1) > digit(z, 1)) cout << "3-Decreasing" << endl;
    else cout << "Nothing" << endl;
  }
  else cout << "Nothing" << endl;
}
