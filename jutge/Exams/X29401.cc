#include <iostream>
using namespace std;

int pow (int b, int e) {
  int res = 1;
  for (int i = 0; i < e; ++i) res *= b;
  return res;
}

int digit(int n, int pos) {
  return (n/pow(10, pos - 1) - (n/pow(10, pos))*10);
}

int main() {
  int x, y, z;
  int increasing = 0;
  while (cin >> x >> y >> z) {
    if (digit(x, 1) == digit(x, 3) and digit(y, 1) == digit(y, 3) and digit(z, 1) == digit(z, 3)) {
      if (digit(x, 1) < digit(y, 1) and digit(y, 1) < digit(z, 1)) ++increasing;
    }
  }
  cout << "3-Increasing: " << increasing << endl;
}
