#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;


struct Info {
    string name;
    int targets;
    int aproxs;
    int fails;
    int points;
};

bool classify(Info& a, Info& b) {
  if (a.points == b.points) {
    if (a.fails == b.fails) return a.name < b.name;
    return a.fails < b.fails;
  }
  return a.points > b.points;
}

vector<Info> read_players(const int& n, const int& k) {
  vector<Info> players(n);
  for (int i = 0; i < n; ++i) {
    cin >> players[i].name >> players[i].targets >> players[i].aproxs;
    players[i].fails = k - players[i].targets - players[i].aproxs;
    players[i].points = players[i].targets*3 + players[i].aproxs;
  }
  return players;
}

void print_player(const Info& player) {
  cout << player.name << ' ' << player.points << ' ' << player.fails << endl;
}

int main() {
  int n, k;
  cin >> n >> k;
  vector<Info> players = read_players(n, k);
  sort(players.begin(), players.end(), classify);
  for (int i = 0; i < n; ++i) print_player(players[i]);
}
