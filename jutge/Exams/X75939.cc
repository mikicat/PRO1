#include <iostream>
#include <vector>
using namespace std;

typedef vector<int> Row;
typedef vector<Row> Matrix;

bool isParchessi(const Matrix& m, int& up, int& down, int& left, int& right) {
  int n = m.size();

  // Upper, Left
  for (int i = 0; i < n/2; ++i) {
    for (int j = i + 1; j < n - i - 1; ++j) up += m[i][j], left += m[j][i];
  }

  // Bottom, Right
  for (int i = n - 1; i > n/2; --i) {
    for (int j = n - i; j < i; ++j) down += m[i][j], right += m[j][i];
  }

  if (up == down and left == right) return true;
  else return false;
}

Matrix get_matrix(int n) {
  Matrix m(n, Row(n));
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) cin >> m[i][j];
  }
  return m;
}

int main() {
  int n;
  while (cin >> n) {
    int up, down, left, right;
    up = down = left = right = 0;
    Matrix m = get_matrix(n);
    bool Parchessi = isParchessi(m, up, down, left, right);

    cout << boolalpha << Parchessi << ": " << up << ' ' << down << ", " << left << ' ' << right << endl;
  }
}
