#include <iostream>
using namespace std;

int main() {
  int x1, x2, x3, x4;
  int increasing = 0;
  int folded = 0;

  while (cin >> x1 >> x2 >> x3 >> x4) {
    if (x1 < x2 and x2 < x3 and x3 < x4) ++increasing;
    else if (x1 == x3 and x2 == x4) ++folded;
  }
  cout << "Increasing: " << increasing << endl;
  cout << "Folded: " << folded << endl;
}
