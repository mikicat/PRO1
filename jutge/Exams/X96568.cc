#include <iostream>
using namespace std;

// Pre: x>0
int product_of_consecutive(int x) {
  int a = 1, b = x;
  int n = (a + b)/2;
  int op = n*(n + 1);

  while (op != x) {
    if (op > x) b = n - 1, n = (a + b)/2;
    else if (op < x) {
      if (a > b) return -1;
      a = n + 1;
      n = (a + b)/2;
    }
    op = n*(n + 1);
  }
  return n;
}

int main() {
   int x;
   while (cin >> x) {
      int k = product_of_consecutive(x);
      if (k == -1) cout << x << " NO" << endl;
      else cout << x << " = " << k << "*" << k+1 << endl;
   }
}
