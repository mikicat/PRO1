#include <iostream>
using namespace std;

int main() {
  int n;
  char c;
  cin >> n >> c;
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j <= i; ++j) {
      if (j == 1) cout << string(i, c) << endl;
      else if (j == i) cout << string(i, c) << endl;
      else cout << c << string(i-2, ' ') << c << endl;
    }
    cout << endl;
  }
}
