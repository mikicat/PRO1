#include <iostream>
using namespace std;

int main() {
  int n;
  int jump = 0;
  int i = 1;

  while (cin >> n) {
    if (i < jump) {
      if (n % 2 == 0) cout << "sobrevolant parell: " << n << endl;
      ++i;
    }
    else {
      cout << "aterrant a: " << n << endl;
      i = 1;
      jump = n;
    }
  }
}
