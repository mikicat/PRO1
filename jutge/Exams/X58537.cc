#include <iostream>
#include <vector>
using namespace std;

typedef vector<int> Row;
typedef vector<Row> Matrix;

// Pre: mat is an n*n square matrix, where n >= 2
// Post: it returns true if mat is a zero-sum tridiagonal matrix,
//       false otherwise
bool is_zerosum_tridiagonal(const Matrix& mat) {
  int sum = 0, main_sum = 0;
  int s = mat.size();

  for (int i = 0; i < s; ++i) {
    for (int j = 0; j < s; ++j) {
      if (i == j) {
        main_sum += mat[i][j];

        if (i + 1 < s) {
          sum += mat[i + 1][j];
        }
        if (i - 1 >= 0) {
          sum += mat[i - 1][j];
        }
      }
      else {
        if (mat[i][j] != 0 and i + 1 != j and i - 1 != j) return false;
      }
    }

  }
  if (sum == main_sum) return true;
  else return false;
}


Matrix read_matrix(int n) {
    Matrix m(n, Row(n));
    for (int i=0; i < n; ++i)
          for (int j = 0; j < n; ++j)
               cin >> m[i][j];
    return m;
}

int main() {
    int n;
    while (cin >> n) {
          Matrix a = read_matrix(n);
          if (is_zerosum_tridiagonal(a)) cout << "TRUE" << endl;
          else cout << "FALSE" << endl;
    }
}
