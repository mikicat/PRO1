#include <iostream>
#include <vector>
using namespace std;

struct Game {
    string home;           // home team
    int shome;             // home team's score
    string visitor;        // visitor team
    int svisitor;        // visitor team's score
};

int scoring_ability(const vector<Game>& v, const string& team) {
  int total = 0;
  for (int i = 0; i < v.size(); ++i) {
    Game game = v[i];
    if (game.home == team) total += game.shome;
    else if (game.visitor == team) total -= game.svisitor;
  }
  return total;
}

// Pre: n is a natural; the input has available the information of n games
// Post: returns a vector with n correctly initialized games
vector<Game> read_games(int n) {
  vector<Game> games(n);
  for (int i = 0; i < n; ++i) {
    cin >> games[i].home >> games[i].shome >> games[i].visitor >> games[i].svisitor;
  }
  return games;
}

int main() {
  int n;
  cin >> n;
  vector<Game> games = read_games(n);

  string team;
  while (cin >> team) {
    int diff = scoring_ability(games, team);
    cout << team << ' ' << diff << endl;
  }
}
