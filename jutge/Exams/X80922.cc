#include <iostream>
using namespace std;

int main() {
  int n;
  cin >> n;
  int total_pos, total_neg, total_neu;
  total_pos = total_neg = total_neu = 0;

  while (n != 0) {
    int pos, neg;
    pos = neg = 0;
    for (int i = 0; i < n; ++i) {
      string w1, w2;
      cin >> w1 >> w2;
      if (w1 == w2 and w1 == "si") ++pos, ++neg;
      else if (w1 == "si") ++pos;
      else if (w2 == "si") ++neg;
    }
    if (pos > neg) ++total_pos;
    else if (neg > pos) ++total_neg;
    else ++total_neu;

    cin >> n;
  }
  cout << total_pos << ' ' << total_neg << ' ' << total_neu << endl;
}
