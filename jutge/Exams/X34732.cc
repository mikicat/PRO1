#include <iostream>
using namespace std;

int main() {
  int rows;
  cin >> rows;

  int valid = 0;
  for (int i = 0; i < rows; ++i) {
    char c;
    bool right = false;
    bool slope1 = false; // If it's the first slope in the sequence
    for (int j = 0; j < 3; ++j) {
      cin >> c;
      if (c == '/' and not slope1) {
        right = true;
        slope1 = true;
      }
      if (c == '-' and slope1) right = false;
    }
    if (right) ++valid;
  }
  cout << valid << endl;
}
