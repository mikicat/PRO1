#include <iostream>
using namespace std;

int main() {
  int n;
  char c1, c2;
  while (cin >> n >> c1 >> c2) {
    for (int i = n; i > 0; --i) {
      for (int j = n-i; j > 0; --j) cout << c1;
      for (int j = i; j > 0; --j) cout << c2;
      cout << endl;
    }
    cout << endl;
  }
}
