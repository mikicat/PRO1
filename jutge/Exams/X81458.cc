#include <iostream>
using namespace std;

int main() {
  int pos = 0;
  int neg = 0;
  int total = 0;
  int n;
  while (cin >> n) {
    if (n > 0) ++pos;
    else if (n < 0) ++ neg;
    ++total;
  }
  cout << "Pos: " << pos << endl;
  cout << "Neg: " << neg << endl;
  cout << "Tot: " << total << endl;
}
