#include <iostream>
using namespace std;


char atbash (char c);

int main() {
    char c;
    while (cin >> c) {
         if ((c >= 'A' and c <= 'Z') or (c >= 'a' and c <= 'z') or (c == '#')) cout << atbash(c);                
    }
    cout << endl;
    
}

char atbash (char c) {
    if (c != '#') {
        if (c > 'Z') c = c - ('a' - 'A');
        c += ('Z' - 'A') + 2 * ('A' - c);
    }
        
    return c;
}
