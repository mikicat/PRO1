#include <iostream>
#include <vector>

using namespace std;

struct Things {
    string name;
    string color;
    string poison;
    string burns;
    double height;
};

typedef vector<Things> Forest;

void read_data(Forest& v) {
    int n;
    cin >> n;
    for (int i = 0; i < n; ++i) {
      Things item;
      cin >> item.name >> item.color >> item.poison >> item.burns >> item.height;
      v.push_back(item);
    }
}

int count(const Forest& v, string name, string color,
          string poison, string burns, double height) {
  int c = 0;
  vector<bool> chk(v.size(), true);

  for (int i = 0; i < v.size(); ++i) {
    if (name != "*") {
      if (v[i].name != name) chk[i] = false;
    }
    if (color != "*") {
      if (v[i].color != color) chk[i] = false;
    }
    if (poison != "*") {
      if (v[i].poison != poison) chk[i] = false;
    }
    if (burns != "*") {
      if (v[i].burns != burns) chk[i] = false;
    }
    if (height != 0) {
      if (v[i].height < height) chk[i] = false;
    }
  }
  for (int i = 0; i < chk.size(); ++i) {
    if (chk[i]) ++c;
  }
  return c;
}

int main() {
    Forest v;
    read_data(v);
    string name, color, poison, burns;
    double height;
    while (cin >> name >> color >> poison >> burns >> height) {
      int c = count(v, name, color, poison, burns, height);
      cout << c << endl;
    }
}
