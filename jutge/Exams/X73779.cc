#include <iostream>
using namespace std;

int main() {
    string f, s;
    char mf, ms;
    cin >> f >> s;
    int wf = 0, ws = 0, tie = 0;
    while (cin >> mf >> ms) {
        if (mf == ms) ++tie;
        else if (mf == 'R') {
            if (ms == 'T') ++wf;
            else ++ws;
        }
        else if (mf == 'T') {
            if (ms == 'P') ++wf;
            else ++ws;
        }
        else {
            if (ms == 'R') ++wf;
            else ++ws;
        }
    }
    cout << f << ' ' << wf << endl;
    cout << s << ' ' << ws << endl;
    cout << "Ties " << tie << endl;
    cout << "Total " << (wf + ws + tie) << endl;
}
