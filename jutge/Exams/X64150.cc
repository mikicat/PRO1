// WA
#include <iostream>
using namespace std;

int main() {
  string name;
  while (cin >> name) {
    int m, p;
    cin >> m >> p;

    int sells = 0, month_sells;
    bool discontinued = false;
    cin >> month_sells;

    while (month_sells != -1) {
      int period_sells = 0;
      discontinued = false;

      for (int i = 0; i < p; ++i) {
        period_sells += month_sells;
        sells += month_sells;
        cin >> month_sells;
      }

      if (period_sells < m) discontinued = true;
    }

    cout << name;
    if (discontinued) cout << " discontinued";
    else cout << ' ' << sells;
    cout << endl;
  }
}
