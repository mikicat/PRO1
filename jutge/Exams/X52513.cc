#include <iostream>
using namespace std;

int main() {
  char c;
  bool right = false;
  bool slope1 = false;
  for (int i = 0; i < 3; ++i) {
    cin >> c;
    if (c == '/' and not slope1) {
      right = true;
      slope1 = true;
    }
    if (c == '-' and slope1) right = false;
  }
  if (right) cout << "right" << endl;
  else cout << "wrong" << endl;
}
