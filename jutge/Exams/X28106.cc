// Doen't work, abandon after making code too complex (simpler versions also failed)

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

struct Parst {
      int index;
      string sub, s;
      vector<int> vap;
};

// Pre: 0 <= k < y.size()
// Post: The result is the first position i>=k where substring x is found in y,
//       or -1 if no such position exists
int substring_from_k (const string& x, int k,  const string& y) {
  int j = k;
  while (j + x.size() <= y.size()) {
    bool found = true;
    for (int i = j; i < x.size() + j; ++i) {
      if (x[i - j] != y[i]) found = false;
    }
    if (found) return j;
    ++j;
  }
  return -1;
}

// Pre: x.size()>0 and y.size()>0
// Post: The result is a vector containing all the positions in y where
//       substring x occurs.
vector<int> substrings(const string& x, const string& y) {
  vector<int> subs;
  for (int i = 0; i < y.size(); ++i) {
    int k = substring_from_k(x, i, y);
    if (k != -1) {
       if ((subs.size() != 0 and subs[subs.size()-1] != k) or (subs.size() == 0)) subs.push_back(k);
    }
  }
  return subs;
}

// Comparison function to sort the output as required
bool comp (const Parst& psa, const Parst& psb) {
  if (psa.s == psb.s) return psa.index < psb.index;
   return psa.sub < psb.sub;
}

int main() {
     vector<Parst> vparst;
     Parst pst;
     pst.index = 1;
     while (cin >> pst.sub >> pst.s) {
        pst.vap = substrings(pst.sub,pst.s);
        vparst.push_back(pst);
        ++pst.index;
     }
     sort(vparst.begin(),vparst.end(),comp);
     int vpn = vparst.size();
     for (int i = 0; i < vpn;++i){
        cout << vparst[i].sub << " " << vparst[i].s;
        int n = vparst[i].vap.size();
        for (int j = 0; j < n; ++ j)
            cout << " " << vparst[i].vap[j];
        cout << endl;
    }
 }
