#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

// 'Subject' stores information about a subject taken by a student:
// name of the subject and obtained grade
struct Subject {
  string name;
  int grade;
};

// 'Student' stores information about a student: id number, university name,
// average grade and list of taken subjects with obtained grades.
struct Student {
  string dni;
  string school;
  float average;
  vector<Subject> subjects;
};

/// YOU CAN ADD EXTRA FUNCTIONS HERE IF YOU NEED TO

// Reads input data and stores info for each student in a struct 'Student'.
// It also computes and stores the average mark for each student.
// Returns a vector with all students.
// Each 'Student' struct should be added to the vector using "push_back".
vector<Student> read_students() {
  vector<Student> stds;
  string dni, school;

  while(cin >> dni >> school) {
    Student stu;
    stu.dni = dni, stu.school = school;

    float av = 0;
    int i = 0;
    Subject subj;
    cin >> subj.name;
    while (subj.name != ".") {
      ++i;
      cin >> subj.grade;
      av += subj.grade;
      stu.subjects.push_back(subj);
      cin >> subj.name;
    }
    av /= i;
    stu.average = av;

    stds.push_back(stu);

  }
  return stds;
}

// Compare two students and return true if s1 goes before s2 in
// the required ordering (sorted by school name, average if same
// school, dni if same school and average)
bool compare_students(const Student &s1, const Student &s2) {
  if (s1.school == s2.school) {
    if (s1.average == s2.average) return s1.dni < s2.dni;
    return s1.average < s2.average;
  }
  return s1.school < s2.school;
}

// Print sorted student information
// For each student, print school name, average grade, dni, list of subjects and marks
void print_students(const vector<Student> &stds) {
  for (int i = 0; i < stds.size(); ++i) {
    Student stu = stds[i];
    cout << stu.school << ' ' << stu.average << ' ' << stu.dni << ' ';
    for (int j = 0; j < stu.subjects.size(); ++j) {
      if (j != 0) cout << ' ';
      cout << stu.subjects[j].name << ' ' << stu.subjects[j].grade;
    }
    cout << endl;
  }
}

int main() {
  vector<Student> stds = read_students();
  sort(stds.begin(), stds.end(), compare_students);
  print_students(stds);
}
