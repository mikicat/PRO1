#include <iostream>
using namespace std;

int countDigits(int n);
int pow(int b, int e);
int getDigit(int n, int pos);

int main() {
  int n;
  while(cin >> n){
    int sum = 0;

    for (int i=1; i <= countDigits(n); ++i) {
      sum += getDigit(n, i);
    }

    cout << "The sum of the digits of " << n << " is " << sum << "." << endl;
  }
}

int countDigits(int n) {
  int digits = 1;
  while(n/10 != 0) {
    ++digits;
    n = n/10;
  }
  return digits;
}

int pow(int b, int e) {
  int res = 1;
  for (int i = 0; i < e; ++i) {
    res *= b;
  }
  return res;
}

int getDigit(int n, int pos) {
  return (n / pow(10, pos-1) - (n / (pow(10, pos))*10));
}
