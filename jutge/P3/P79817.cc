#include <iostream>
using namespace std;

int main() {
  int b, e;
  while(cin >> b >> e){
    int res = 1;
    for (int i = 0; i < e; ++i) {
      res *= b;
    }

    cout << res << endl;
  }
}
