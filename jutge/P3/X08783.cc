#include <iostream>
using namespace std;

int main() {
  int n, b;
  while(cin >> b >> n) {
    int digits = 0;
    if (n == 0) ++digits;
    while(n != 0) {
      ++digits;
      n /= b;
    }
    cout << digits << endl;

  }
}
