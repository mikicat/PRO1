#include <iostream>
using namespace std;

int main() {
  int n;
  while (cin >> n) {
    int steps = 0;
    while (n != 1) {
      if (n % 2) n = n * 3 + 1;
      else n /= 2;
      ++steps;
    }
    cout << steps << endl;
  }

}
