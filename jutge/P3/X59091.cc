#include <iostream>
using namespace std;

int main() {
  int n, m;
  bool first_run = true;
  while (cin >> n >> m) {
    int fill = 9;
    if (not first_run) cout << endl;
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        cout << fill;
        if (fill == 0) fill = 9;
        else --fill;
      }
      cout << endl;
    }
    first_run = false;
  }
}
