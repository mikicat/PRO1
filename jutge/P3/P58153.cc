// time limit exceeded (EE)
#include <iostream>
using namespace std;

int main() {
  int n, m;
  while (cin >> n >> m) {
    cout.precision(10);
    cout.setf(ios::fixed);
    if (n == m) cout << double(0) << endl;
    else {
      double h = 0;
      for(int i = m + 1; i <= n; ++i) h += 1.0 / i; // m +1 pq si m == 0 -> 1.0/0 = infinit
      cout << h << endl;
    }

  }
}
