#include <iostream>
using namespace std;

int main() {
  int b, n;
  cin >> b;
  while (cin >> n) {
    int x = 0;
    for (int i = n; i != 0; i /= b)
    {
      x += i % b;
    }
    cout << n << ": " << x << endl;
  }
}
