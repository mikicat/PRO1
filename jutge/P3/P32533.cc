#include <iostream>
using namespace std;

int main() {
  int times;
  cin >> times;
  for(int i = 1; i <= times; ++i) {
    for(int j = 0; j < times-i; ++j) {
      cout << '+';
    }
    cout << '/';
    for(int k = i-1; k > 0; --k) {
      cout << '*';
    }
    cout << endl;
  }
}
