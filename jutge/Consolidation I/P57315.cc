#include <iostream>
using namespace std;

int maxim(int a, int b) {
  if (a >= b) return a;
  else return b;
}
int minim(int a, int b) {
  if (a <= b) return a;
  else return b;
}
int main() {
  int a, b, c;
  cin >> a >> b >> c;
  char order;
  int max = maxim (a, b);
  if (max < c) max = c;
  int min = minim (a, b);
  if (min > c) min = c;
  int mid;

  if (max != a and min != a) mid = a;
  else if (max != b and min != b) mid = b;
  else mid = c;

  for (int i = 0; i < 3; ++i) {
    cin >> order;
    int res = order - 64;
    if (res == 1) cout << min;
    else if (res == 2) cout << mid;
    else cout << max;
    if (i < 2) cout << ' ';
  }
  cout << endl;
}
