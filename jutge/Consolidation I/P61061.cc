#include <iostream>
using namespace std;

int main() {
    int n;
    while (cin >> n) {
        int product = 1;
        while (product != 0) {
            if (n != 0) product = 1;
            else product = 0;
            for (int i = n; i > 0; i /= 10) {
                product *= i%10;
            }
            cout << "The product of the digits of " << n << " is " << product << '.' << endl;
            if (n == product or product/10 == 0) product = 0;
            else n = product;
        }
        for (int j = 0; j < 10; ++j) cout << '-';
        cout << endl;
    }
}
