#include <iostream>
using namespace std;

int main() {
    int n;
    while (cin >> n) {
        int sum_7 = 0;
        int sum_4 = 0;
        while (n != 0) {
            if (n%7 == 0) {
                sum_7 = n/7;
                n = 0;
            }
            else if (n%4 == 0) {
                sum_4 = n/4;
                n = 0;
            }
            else {
                ++sum_7;
                n %= 7;
                ++sum_4;
                n %= 4;
            }
        }
        if (sum_4/7 != 0) {
            sum_7 += sum_4/7;
            sum_4 %= 7;
        }
        cout << sum_7 << ' ' << sum_4 << endl;
    }
}
