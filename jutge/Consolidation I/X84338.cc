#include <iostream>
using namespace std;

int digits(string n) {
  int sum = 0, i = 0;
  while (n[i] != '\0') {
    ++sum;
    ++i;
  }
  return sum;
}

int main() {
  string n;
  cin >> n;
  int length = digits(n);
  if (length % 2 != 0 or int(n[0]-'0') == 0) cout << "nothing" << endl;
  else {
    int first = 0, second = 0;
    for(int i = 0; i < length / 2; ++i) {
      first += int(n[i] - '0');
      second += int(n[i+length/2] - '0');
    }
    if (first == second) cout << first << " = " << second << endl;
    else if (first > second) cout << first << " > " << second << endl;
    else cout << first << " < " << second << endl;
  }
}
