#include <iostream>
using namespace std;

int main() {
    cout.precision(4);
    cout.setf(ios::fixed);
    double x;
    cin >> x;
    double sum = 0;
    double res = 1;
    double c;
    while(cin >> c) {
        sum += c * res;
        res *= x;
    }
    
    cout << sum << endl;
    
}
