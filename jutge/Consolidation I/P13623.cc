#include <iostream>
using namespace std;

int main() {
  int r, c;
  cin >> r >> c;
  int sum = 0;
  for (int i = 0; i < r; ++i) {
    string coins;
    cin >> coins;
    bool even = not(i % 2);
    for (int j = 0; j < c; ++j) {
      if (even) {
        if(not(j % 2)) sum += int(coins[j] - 48);
      }
      else {
        if (j % 2) sum += int(coins[j] - 48);
      }
    }
  }

  cout << sum << endl;

}
