#include <iostream>
using namespace std;

int main() {
  string first, word;
  cin >> first;
  int max = 1;
  bool first_run = true;

  while (cin >> word)
  {
    int i = 0;
    if (word == first) {
      if (first_run) i = 2;
      else ++i;
    }

    while (cin >> word and word == first) ++i;
    if (i > max) max = i;

    first_run = false;
  }

  cout << max << endl;
}
