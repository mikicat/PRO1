#include <iostream>
using namespace std;

int main() {
  string p, next;
  int n;
  int c = 1;
  bool increasing = false;
  while (cin >> n and not increasing) {
    increasing = true;
    cin >> p;
    for (int i = 2; i <= n; ++i) {
      cin >> next;
      if (next < p) increasing = false;
      p = next;
    }
    if (increasing) cout << "The first line in increasing order is " << c << "." << endl;
    ++c;
  }
  if (not increasing) cout << "There is no line in increasing order." << endl;
}
