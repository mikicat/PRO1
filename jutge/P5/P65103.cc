#include <iostream>
using namespace std;

int main() {
  int pos;
  cin >> pos;
  int i = 1;
  int n, k;
  bool found = false;
  while (cin >> n and not found) {
    if (i == pos) {
      found = true;
      k = n;
    }
    ++i;
  }

  if (found) cout << "At the position " << pos << " there is a(n) " << k << '.' << endl;
  else cout << "Incorrect position." << endl;
}
