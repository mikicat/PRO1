#include <iostream>
using namespace std;

int main() {
  int n;
  while (cin >> n) {
    int x;
    cin >> x;
    int max = x;
    for (int i = 1; i < n; ++i) {
      cin >> x;
      if (x > max) max = x;
    }
    cout << max << endl;
  }
}
