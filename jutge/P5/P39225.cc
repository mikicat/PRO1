#include <iostream>
using namespace std;

int main() {
	int pos;
	cin >> pos;
	int n, i = 1;
	while (cin >> n and i != pos) ++i;
	
	cout << "At the position " << pos << " there is a(n) " << n << '.' << endl;

}