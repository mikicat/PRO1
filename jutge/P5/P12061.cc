#include <iostream>
using namespace std;

int main() {
  string word;
  bool begin = false, end = false;
  int i = 0;
  while (cin >> word and not end) {
    if (begin and word != "end") ++i;

    if (begin and word == "beginning") {
      begin = false;
      end = true;
    }
    else if (word == "beginning") begin = true;
    else if (word == "end") end = true;
  }
  if (begin and end) cout << i << endl;
  else cout << "wrong sequence" << endl;
}
