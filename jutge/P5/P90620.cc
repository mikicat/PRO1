#include <iostream>
using namespace std;

int main() {
  int x, y, z;
  bool peak = false;
  cin >> x;
  cin >> y;
  while (cin >> z and z != 0) {
    if (y > 3143 and x < y and z < y) peak = true;
    x = y;
    y = z;
  }
  if (peak) cout << "YES" << endl;
  else cout << "NO" << endl;
}
