#include <iostream>
using namespace std;

int main() {
  int n, y, x = 0, c = 0;
  bool f = true;
  cin >> n;

  while (cin >> y) {
    if (y != 0) {
      if (not f and y > x) ++c;
      x = y, f = false;
    }
    else {
      cout << c << endl;
      c = 0, f = true;
    }
  }
}
