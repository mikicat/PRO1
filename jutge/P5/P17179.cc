#include <iostream>
using namespace std;


int main() {
  cout.setf(ios::fixed);
  cout.precision(4);
  int n;
  cin >> n;
  for (int i = 0; i < n; ++i) {
    double m, num, min, max, av;
    cin >> m;
    cin >> num;
    min = max = av = num;
    for (int j = 0; j < m-1; ++j) {
      cin >> num;
      if (num < min) min = num;
      if (num > max) max = num;
      av += num;
    }
    av /= m;
    cout << min << ' ' << max << ' ' << av << endl;
  }
}
