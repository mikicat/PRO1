#include <iostream>
using namespace std;

bool is_prime (int n) {
  if (n <= 1) return false;
  for (int i = 2; i*i <= n; ++i) {
    if (n % i == 0) return false;
  }
  return true;
}

int main() {
  int n;
  while (cin >> n and is_prime(n)) {
    bool next_prime = false;
    while (not next_prime) {
      ++n;
      next_prime = is_prime(n);
    }

    cout << n << endl;
  }
}
