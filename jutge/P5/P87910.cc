#include <iostream>
using namespace std;

void morse(char c, int& n) {
	string code;	
	if (c == 'a') code = ".-";
	else if (c == 'e') code = ".";
	else if (c == 'i') code = "..";
	else if (c == 'o') code = "---";
	else if (c == 'u') code = "..-";
	
	cout << code;
	n += code.length();
}

int main() {
	int m, n = 0;
	char c;
	cin >> m;
	while (cin >> c) {
		morse(c, n);
		if (n >= m) {
			n = 0;
			cout << endl;
		}
	}
	if (n != 0) cout << endl;
	cout << "STOP" << endl;
}