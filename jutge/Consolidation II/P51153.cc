#include <iostream>
using namespace std;

int main() {
  int n;
  while (cin >> n) {
    int x, y;
    if (n == 0) cout << "no" << endl;
    else {
      cin >> x;
      bool odd = false;
      int i = 1;
      while (i < n and not(odd)) {
        cin >> y;
        if ((y+x)%2 != 0) odd = true;
        y = x;
        ++i;
      }
      if (odd) cout << "yes" << endl;
      else cout << "no" << endl;
    }
  }
}
