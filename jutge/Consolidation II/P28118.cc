#include <iostream>
#include <cmath>
using namespace std;

void route(string place) {
  double x, y;
  double x_prev, y_prev;
  cin >> x_prev >> y_prev;

  double total = 0;
  cin >> x >> y;
  double f_x = x_prev, f_y = y_prev, t_x = 0, t_y = 0;

  while ((x != f_x) or (y != f_y)) {
    t_x = 0, t_y = 0;
    t_x = x - x_prev;
    t_y = y - y_prev;

    x_prev = x;
    y_prev = y;

    total += sqrt((t_x * t_x) + (t_y * t_y));
    cin >> x >> y;
  }
  // CAS SEGÜENT
  t_x = x - x_prev;
  t_y = y - y_prev;

  total += sqrt((t_x * t_x) + (t_y * t_y));

  cout << "Route " << place << ": " << total << endl;
}

int main() {
  cout.setf(ios::fixed);
  cout.precision(4);

  string place;
  while (cin >> place) {
    route(place);
  }
}
