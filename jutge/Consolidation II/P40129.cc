#include <iostream>
using namespace std;

int main() {
  char c;
  while (cin >> c) {
    int pos = 0;
    int total = 0;
    if (c == '?') pos = 10;
    else total += 10*int(c - '0');

    for (int i = 9; i > 0; --i) {
      cin >> c;
      if (c == '?') pos = i;
      else if (c == 'X') total += 10;
      else total += i*int(c - '0');
    }

    int x = 0;
    for (int i = 0; i <= 10; ++i) {
      if ((total + i*pos)%11 == 0) x = i;
    }

    if (x == 10) cout << 'X' << endl;
    else cout << x << endl;
  }
}
