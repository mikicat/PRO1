#include <iostream>
using namespace std;

int calc() {
  char c;
  cin >> c;
  if (c >= '0' and c <= '9') return int(c - '0');
  else {
    int res;
    int expr1 = calc();
    char op;
    cin >> op;
    int expr2 = calc();
    char p;
    cin >> p;
    if (op == '+') res = expr1 + expr2;
    else if (op == '-') res = expr1 - expr2;
    else res = expr1*expr2;

    return res;
  }
}

int main() {
  cout << calc() << endl;
}
