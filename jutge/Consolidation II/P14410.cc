#include <iostream>
using namespace std;

void converge(int n, int& k, int& far) {
  far = n;
  while (n != 1) {
    if (n%2 == 0) n /= 2;
    else n = 3*n + 1;
    if (n > far) far = n;
    ++k;
  }
}

int main() {
  int m,p;
  cin >> m >> p;

  int furthest = 1;
  for (int i = 1; i <= m; ++i) {
    int far = 0, k = 0;
    converge(i, k, far);
    if (k >= p) cout << i << endl;
    if (far > furthest) furthest = far;
  }

  cout << "The greatest reached number is " << furthest << '.' << endl;
}
