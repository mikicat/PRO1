// NO VA BE

#include <iostream>
using namespace std;

int main() {
  cout.setf(ios::fixed);
  cout.precision(10);
  int n;
  while (cin >> n) {
    double res = 0;
    for (double i = 1; i <= n; ++i) {
      double factor = 1;
      for (int j = 1; j <= i; ++j) factor *= j;
      cout << factor << endl;
      res += (1/factor);
    }

    cout << "With " << n << " term(s) we get " << res << '.' << endl;
  }
}
