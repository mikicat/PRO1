#include <iostream>
using namespace std;

int main() {
  char c;
  int s = 0, mid = 0, l = 0;
  while (cin >> c) {
    int i = 0;
    while ((c >= 'A' and c <= 'Z') or (c >= 'a' and c <= 'z')) {
      ++i;
      cin >> c;
    }
    if (i > 0 and i < 5) ++s;
    else if (i >= 5 and i <= 9) ++mid;
    else if (i > 9) ++l;
  }

  cout << s << ',' << mid << ',' << l << endl;
}
