#include <iostream>
#include <vector>
using namespace std;
typedef vector< vector<int> > Matrix;

void transpose(Matrix& m) {
    Matrix transposed(m.size(), vector<int>(m.size()));
    
    for (int i = 0; i < m.size(); ++i) {
        for (int j = 0; j < m.size(); ++j) {
            transposed[i][j] = m[j][i];
        }
    }
    m = transposed;
}

int main() {
    int n;
    cin >> n;
    Matrix m(n, vector<int>(n));
    
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) cin >> m[i][j];
    }
    transpose(m);
}
