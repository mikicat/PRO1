#include <iostream>
#include <vector>
using namespace std;
typedef vector<int> Row;
typedef vector< Row > Matrix;

int main() {
    int n, m;
    cin >> n >> m;
    Matrix matrix(n, Row(m));
    
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) cin >> matrix[i][j];
    }
    
    string command;
    while (cin >> command) {
        if (command == "row") {
            int index;
            cin >> index;
            cout << "row " << index << ": ";
            --index;
            
            for (int i = 0; i < m; ++i) {
                cout << matrix[index][i];
                if (i != m - 1) cout << ' ';
            }
        }
        else if (command == "column") {
            int index;
            cin >> index;
            cout << "column " << index << ": ";
            --index;
            
            for (int i = 0; i < n; ++i) {
                cout << matrix[i][index];
                if (i != n - 1) cout << ' ';
            }
        }
        else if (command == "element") {
            int row, col;
            cin >> row >> col;
            cout << "element " << row << ' ' << col << ": ";
            --row;
            --col;
            cout << matrix[row][col];
        }
        cout << endl;
    }
}
