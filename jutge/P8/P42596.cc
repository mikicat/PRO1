#include <iostream>
#include <vector>
using namespace std;

typedef vector<vector<int> > Matrix;

void min_max(const Matrix& mat, int& minimum, int& maximum) {
  minimum = maximum = mat[0][0];
  for (int i = 0; i < mat.size(); ++i) {
    for (int j = 0; j < mat[0].size(); ++j) {
      if (mat[i][j] > maximum) maximum = mat[i][j];
      else if (mat[i][j] < minimum) minimum = mat[i][j];
    }
  }
}

int main() {
  int x, y;
  int max_dif = 0, i = 1, max_i = i;
  while (cin >> x >> y) {
    int minimum, maximum;
    Matrix mat(x, vector<int>(y));

    for (int j = 0; j < x; ++j) {
      for (int k = 0; k < y; ++k) cin >> mat[j][k];
    }
    min_max(mat, minimum, maximum);

    if (maximum - minimum > max_dif) {
      max_dif = maximum - minimum;
      max_i = i;
    }
    ++i;
  }

  cout << "la diferencia maxima es " << max_dif << endl;
  cout << "la primera matriu amb aquesta diferencia es la " << max_i << endl;
}
