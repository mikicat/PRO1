#include <iostream>
#include <vector>
using namespace std;

typedef vector< vector<int> > Matrix;
const int LEN = 9;

bool check_v(Matrix& sudoku, int x, int y) {
  for (int i = 0; i < LEN; ++i) {
    if (i != x and sudoku[i][y] == sudoku[x][y]) return false;
  }
  return true;
}

bool check_h(Matrix& sudoku, int x, int y) {
  for (int i = 0; i < LEN; ++i) {
    if (i != y and sudoku[x][i] == sudoku[x][y]) return false;
  }
  return true;
}

bool check_square(Matrix& sudoku, int x, int y) {
  int init_x = (x / 3) * 3;
  int init_y = (y / 3) * 3;
  for (int i = init_x; i < init_x + 3; ++i) {
    for (int j = init_y; j < init_y + 3; ++j) {
      if ((i != x) and (j != y) and (sudoku[x][y] == sudoku[i][j])) return false;
    }
  }
  return true;
}

bool check(Matrix& sudoku) {
  for (int i = 0; i < LEN; ++i) {
    for (int j = 0; j < LEN; ++j) {
      if (not(check_v(sudoku, i, j))) return false;
      if (not(check_h(sudoku, i, j))) return false;
      if (not(check_square(sudoku, i, j))) return false;
    }
  }
  return true;
}

int main() {
  int n;
  cin >> n;
  for (int i = 0; i < n; ++i) {
    Matrix sudoku(LEN, vector<int>(LEN));
    for (int j = 0; j < LEN; ++j) {
      for (int k = 0; k < LEN; ++k) cin >> sudoku[j][k];
    }
    if (check(sudoku)) cout << "yes" << endl;
    else cout << "no" << endl;
  }
}
