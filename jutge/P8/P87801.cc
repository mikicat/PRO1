#include <iostream>
#include <vector>
using namespace std;

typedef vector< vector<int> > Matrix;
typedef vector< vector<char> > Soup;

int match_h(const Soup& soup, const Matrix& values, string& w, int& i, int& j) {
  int len = w.length();
  int value = 0;
  if (len > soup[0].size() - j) return -1;
  for (int l = 0; l < len; ++l) {
    value += values[i][j+l];
    if (w[l] != soup[i][j+l]) return -1;
  }
  return value;
}

int match_v(const Soup& soup, const Matrix& values, string& w, int& i, int& j) {
  int len = w.length();
  int value = 0;
  if (len > soup.size() - i) return -1;
  for (int l = 0; l < len; ++l) {
    value += values[i+l][j];
    if (w[l] != soup[i+l][j]) return -1;
  }
  return value;
}

int search(const Soup& soup, const Matrix& values, string& w) {
  int max_value = 0;
  for (int i = 0; i < soup.size(); ++i) {
    for (int j = 0; j < soup[0].size(); ++j) {
      if (soup[i][j] == w[0]) {
        int val = match_h(soup, values, w, i, j);
        if (val > max_value) max_value = val;
        val = match_v(soup, values, w, i, j);
        if (val > max_value) max_value = val;
      }
    }
  }
  return max_value;
}

int main() {
  int x, y;
  while (cin >> x >> y) {
    // Setting up the Matrix
    Soup soup(x, vector<char>(y));
    for (int i = 0; i < x; ++i) {
      for (int j = 0; j < y; ++j) cin >> soup[i][j];
    }
    Matrix values(x, vector<int>(y));
    for (int i = 0; i < x; ++i) {
      for (int j = 0; j < y; ++j) cin >> values[i][j];
    }

    int k;
    cin >> k;
    for (int i = 0; i < k; ++i) {
      string w;
      cin >> w;
      int max = search(soup, values, w);
      if (max > 0) cout << max << endl;
      else cout << "no" << endl;

    }
  }
}
