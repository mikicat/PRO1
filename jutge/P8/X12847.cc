// A mig
#include <iostream>
#include <vector>
using namespace std;

typedef vector< vector<bool> > Board;

int min(int a, int b) {
	if (a <= b) return a;
	else return b;
}
int max(int a, int b) {
	if (a >= b) return a;
	else return b;
}

bool find_h(const Board& board, int i, int y0, int y) {
  for (int j = y0; j <= y; ++j) {
    if (board[i][j]) return true;
  }
  return false;
}

bool find_v(const Board& board, int j, int x0, int x) {
  for (int i = x0; i <= x; ++i) {
    if (board[i][j]) return true;
  }
  return false;
}

int min_dist(Board& board, int x, int y) {
  int dist = 1;
  int n = board.size();

  while (dist <= 10) {
    if (x-dist >= 0) {
      if (find_h(board, x-dist, max(0, y-dist), min(y+dist, n-1))) return dist;
    }
    if (x+dist < n) {
      if (find_h(board, x+dist, max(0, y-dist), min(y+dist, n-1))) return dist;
    }
    if (y-dist >= 0) {
      if (find_v(board, y-dist, max(0, x-dist), min(x+dist, n-1))) return dist;
    }
    if (y+dist < n) {
      if (find_v(board, y+dist, max(0, x-dist), min(x+dist, n-1))) return dist;
    }
    ++dist;
  }
  return dist;
}

void draw_board(Board& board) {
  cout << "  ";
  for (int i = 1; i <= 10; ++i) cout << i;
  cout << endl;
  for (int i = 0; i < 10; ++i) {
    cout << char(i + 'a') << ' ';
    for (int j = 0; j < 10; ++j) {
      if (board[i][j]) cout << 'X';
      else cout << '.';
    }
    cout << endl;
  }
  cout << endl;
}

int main() {
  Board board (10, vector<bool>(10, false));
  for (int i = 0; i < 10; ++i) {
    char letter, pos;
    int y, width;
    cin >> letter >> y >> width >> pos;
    int x = int(letter - 'a');
    --y;
    for (int j = 0; j < width; ++j) {
      if (pos == 'h') board[x][y+j] = true;
      else board[x+j][y] = true;
    }
  }

  draw_board(board);

  char letter;
  int y;
  while (cin >> letter >> y) {
    int i = int(letter - 'a'), j = y - 1;
    if (board[i][j]) cout << letter << y << " touched!" << endl;
    else {
      cout << letter << y << " water! closest ship at distance ";
      cout << min_dist(board, i, j);
      cout << endl;
    }
  }


}
