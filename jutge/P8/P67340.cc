// A mig
#include <iostream>
#include <vector>
using namespace std;
typedef vector< vector<bool> > Board;
typedef vector< vector<int> > Matrix;

bool d_up_l(const Board& board, int i, int j, int& x, int& y) {
    int n = board[0].size();
    int m = board.size();

    x = i - 1;
    y = j - 1;
    while (((x < m) and (x >= 0)) and ((y < n) and (y >= 0))) {
        if (board[x][y]) return true;
        --x;
        --y;
    }
    return false;
}

bool d_up_r(const Board& board, int i, int j, int& x, int& y) {
    int n = board[0].size();
    int m = board.size();

    x = i - 1;
    y = j + 1;
    while (((x < m) and (x >= 0)) and ((y < n) and (y >= 0))) {
        if (board[x][y]) return true;
        --x;
        ++y;
    }
    return false;
}

bool d_down_l(const Board& board, int i, int j, int& x, int& y) {
    int n = board[0].size();
    int m = board.size();

    x = i + 1;
    y = j - 1;
    while (((x < m) and (x >= 0)) and ((y < n) and (y >= 0))) {
        if (board[x][y]) return true;
        ++x;
        --y;
    }
    return false;
}

bool d_down_r(const Board& board, int i, int j, int& x, int& y) {
    int n = board[0].size();
    int m = board.size();

    x = i + 1;
    y = j + 1;
    while (((x < m) and (x >= 0)) and ((y < n) and (y >= 0))) {
        if (board[x][y]) return true;
        ++x;
        ++y;
    }
    return false;
}

void check(Board& board) {
  for (int i = 0; i < board.size(); ++i) {
    for (int j = 0; j < board[0].size(); ++j) {
      if (board[i][j]) {
        int x, y;
        if (d_up_l(board, i, j, x, y)) {
          cout << '(' << i + 1 << ',' << j + 1 << ")<->(" << x + 1 << ',' << y + 1 << ')' << endl;
        }
        if (d_up_r(board, i, j, x, y)) {
          cout << '(' << i + 1 << ',' << j + 1 << ")<->(" << x + 1 << ',' << y + 1 << ')' << endl;
          x = i, y = j;
        }
        if (d_down_l(board, i, j, x, y)) {
          cout << '(' << i + 1 << ',' << j + 1 << ")<->(" << x + 1 << ',' << y + 1 << ')' << endl;
          x = i, y = j;
        }
        if (d_down_r(board, i, j, x, y)) {
          cout << '(' << i + 1 << ',' << j + 1 << ")<->(" << x + 1 << ',' << y + 1 << ')' << endl;
          x = i, y = j;
        }
      }
    }
  }
}

int main() {
  int x, y;
  cin >> x >> y;
  Board board(x, vector<bool>(y, false));
  for(int i = 0; i < x; ++i) {
    for (int j = 0; j < y; ++j) {
      char c;
      cin >> c;
      if (c == 'X') board[i][j] = true;
    }
  }
  check(board);

}
