#include <iostream>
#include <vector>
using namespace std;

typedef vector< vector<int> > Matrix;

int main() {
  int x, y;
  while (cin >> x >> y) {
    Matrix field(x, vector<int>(y));
    for (int i = 0; i < x; ++i) {
      for (int j = 0; j < y; ++j) cin >> field[i][j];
    }
    int total = 0;
    vector<bool> active(y, false);
    bool start = true;
    for (int i = 0; i < x; ++i) {
      for (int j = 0; j < y; ++j) {
        if (start and field[i][j] != 0) {
          active[j] = true;
          start = false;
        }
        else if (field[i][j] == 0) {
          start = true;
          if(active[j]) {
            active[j] = false;
            ++total;
          }
        }
      }
    }
    for (int i = 0; i < y; ++i) {
      if (active[i]) ++total;
    }
    cout << total << endl;
  }
}
