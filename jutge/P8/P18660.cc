#include <iostream>
#include <vector>
using namespace std;
typedef vector< vector<char> > Soup;


char to_upper(char c) {
  if (c < 'a') return c;
  else return c - ('a' - 'A');
}

bool match_h(vector<int>& type, string& name, Soup& soup, int j, int k) {
  int len = name.length(), l = 0;
  while (l < (len - 1)) {
    ++l;
    if (to_upper(name[l]) != to_upper(soup[j][k+l])) return false;
  }
  type[0] = 0;
  type[1] = l;
  return true;
}

bool match_v(vector<int>& type, string& name, Soup& soup, int j, int k) {
  int len = name.length(), l = 0;
  while (l < (len - 1)) {
    ++l;
    if (to_upper(name[l]) != to_upper(soup[j+l][k])) return false;
  }
  type[0] = 1;
  type[1] = l;
  return true;
}

bool match_d(vector<int>& type, string& name, Soup& soup, int j, int k) {
  int len = name.length(), l = 0;
  while (l < (len - 1)) {
    ++l;
    if (to_upper(name[l]) != to_upper(soup[j+l][k+l])) return false;
  }
  type[0] = 2;
  type[1] = l;
  return true;
}

void transform(vector<int>& type, Soup& soup, int j, int k) {
  int l = type[1];
  for (int m = 0; m <= l; ++m) {
    if (type[0] == 0) soup[j][k+m] = to_upper(soup[j][k+m]);
    else if (type[0] == 1) soup[j+m][k] = to_upper(soup[j+m][k]);
    else if (type[0] == 2) soup[j+m][k+m] = to_upper(soup[j+m][k+m]);
  }
}

int main() {
  int n;
  bool first = true;
  while (cin >> n) {
    int x, y;
    cin >> x >> y;
    vector<string> names(n);
    for (int i = 0; i < n; ++i) cin >> names[i];
    Soup soup(x, vector<char>(y));
    for (int i = 0; i < x; ++i) {
      for (int j = 0; j < y; ++j) {
        char c;
        cin >> c;
        soup[i][j] = c;
      }
    }

    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < x; ++j) {
        for (int k = 0; k < y; ++k) {

          if (to_upper(names[i][0]) == to_upper(soup[j][k])) {
            string name = names[i];
            vector<int> type(2, 0);

            if (name.length() <= y - k or name.length() <= x - j) {
              if (name.length() > y - k) {
                if (match_v(type, name, soup, j, k)) {
                  transform(type, soup, j, k);
                }
              }
              else if (name.length() > x - j) {
                if (match_h(type, name, soup, j, k)) {
                  transform(type, soup, j, k);
                }
              }
              else {
                if (match_v(type, name, soup, j, k)) {
                  transform(type, soup, j, k);
                }
                if (match_h(type, name, soup, j, k)) {
                  transform(type, soup, j, k);
                }
                if (match_d(type, name, soup, j, k)) {
                  transform(type, soup, j, k);
                }
              }
            }
          }
        }
      }
    }

    if (not first) cout << endl;
    else first = false;

    for (int i = 0; i < x; ++i) {
      for (int j = 0; j < y; ++j) {
        cout << soup[i][j];
        if (j != y - 1)cout << ' ';
      }
      cout << endl;
    }
  }
}
