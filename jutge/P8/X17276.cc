// No ACABat
#include <iostream>
#include <vector>
using namespace std;
typedef vector< vector<int> > Matrix;

bool check(Matrix& matrix, int x0, int y0, int x, int y) {
  int i = x0, j = y0;
  int n = matrix[x0][y0];
  while (i < x-1 and j < y-1) {
    ++i, ++j;
    if (matrix[i][j] <= n) return false;
    n = matrix[i][j];

  }
  i = x0, j = y0, n = matrix[x0][y0];
  while (i > 0 and j < y-1) {
    --i, ++j;
    if (matrix[i][j] <= n) return false;
    n = matrix[i][j];

  }
  i = x0, j = y0, n = matrix[x0][y0];
  while (i > 0 and j > 0) {
    --i, --j;
    if (matrix[i][j] <= n) return false;
    n = matrix[i][j];

  }
  i = x0, j = y0, n = matrix[x0][y0];
  while (i < x-1 and j > 0) {
    ++i, --j;
    if (matrix[i][j] <= n) return false;
    n = matrix[i][j];

  }
  return true;
}

int main() {
  int x, y;
  while (cin >> x >> y) {
    Matrix matrix(x, vector<int>(y));
    for (int i = 0; i < x; ++i) {
      for (int j = 0; j < y; ++j) cin >> matrix[i][j];
    }
    int x0, y0;
    cin >> x0 >> y0;
    if (check(matrix, x0, y0, x, y)) cout << "yes" << endl;
    else cout << "no" << endl;
  }
}
