#include <iostream>
using namespace std;

void decompose(int n, int& h, int& m, int& s);

int main() {
  int h, m, s;
  int n = 3661;
  decompose(n, h, m, s);
  cout << h << ' ' << m << ' ' << s << endl;
}

void decompose(int n, int& h, int& m, int& s) {
  h = n/3600;
  m = (n%3600)/60;
  s = n%60;
}
