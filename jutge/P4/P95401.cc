#include <iostream>
using namespace std;

bool is_leap_year(int year) {
  bool leap = false;

  if (!(year % 100) and !((year / 100) % 4)) leap = true;
  else if (year % 4 == 0 and (year % 100)) leap = true;
  return leap;
}
int main() {}
