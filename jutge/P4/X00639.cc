#include <iostream>
using namespace std;

bool c_frac(int n1, int d1, int n2, int d2);

int main() {
  int x, y, z, a;
  while (cin >> x >> y >> z >> a) cout << c_frac(x, y, z, a) << endl;
}

bool c_frac(int n1, int d1, int n2, int d2) {
  if (n1 * d2 < n2 * d1) return true;
  else return false;
}
