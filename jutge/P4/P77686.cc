#include <iostream>
using namespace std;

int number_of_digits(int n) {
  int digits = 0;
  if (n == 0) ++digits;
  while (n != 0) {
    ++digits;
    n /= 10;
  }
  return digits;
}
int pow (int b, int e) {
  int res = 1;
  for (int i = 0; i < e; ++i) res *= b;
  return res;
}

bool is_palindromic(int n) {
  bool is_pal = false;
  int pal = 0;
  for (int i = n; i != 0; i /= 10) {
    pal += i % 10 * pow(10, number_of_digits(i)-1);
  }
  if (pal == n) is_pal = true;
  return is_pal;
}

int main() {}
