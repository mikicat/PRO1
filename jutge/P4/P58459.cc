#include <iostream>
using namespace std;

bool is_leap_year(int year) {
  bool leap = false;
  if (!(year % 100) and !((year / 100) % 4)) leap = true;
  else if (year % 4 == 0 and (year % 100)) leap = true;
  return leap;
}
bool is_valid_date(int d, int m, int y) {
  bool valid = true;
  if (d < 1 or d > 31 or m < 1 or m > 12 or y < 1800 or y > 9999) valid = false;
  else if (m > 7 and (m % 2) and d > 30) valid = false;
  else if (m <= 7 and !(m % 2) and d > 30) valid = false;
  else if (m == 2) {
    if (is_leap_year(y) and d > 29) valid = false;
    else if (not is_leap_year(y) and d > 28) valid = false;
  }

  return valid;
}
int main() {}
