#include <iostream>
using namespace std;

void factor(int n, int& f, int& q);

int main() {
  int f, q, n;
  cin >> n;
  factor(n, f, q);
  cout << f << ' ' << q << endl;
}

void factor(int n, int& f, int& q) {
  int d = 2;
  int x = q = 0;
  while (d*d <= n) {
    x = 0;
    while (n%d == 0) {
      ++x;
      n /= d;
      if (x > q) q = x, f = d;
    }
    ++d;
  }
  if (q == 0) f = n, q = 1;
}
