#include <iostream>
#include <cmath>
using namespace std;

int main() {
  double n;
  while (cin >> n) {
    cout.precision(6);
    n = n * M_PI / 180;
    cout << fixed << sin(n) << ' ' << cos(n) << endl;
  }
}
