#include <iostream>
using namespace std;

int number_of_digits(int n) {
  int digits = 0;
  if (n == 0) ++digits;
  while (n != 0) {
    ++digits;
    n /= 10;
  }
  return digits;
}

int main() {}
