#include <iostream>
#include <cmath>
using namespace std;

int main() {
  double n;
  while(cin >> n) {
    cout.precision(6);
    cout << fixed << int(n * n) << " " << sqrt(n) << endl;
  }
}
