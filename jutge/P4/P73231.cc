#include <iostream>
using namespace std;

int max4(int a, int b, int c, int d){
  if (a < b) a = b;
  if (a < c) a = c;
  if (a < d) a = d;
  return a;  
}


int main() {
  int a, b, c, d;
  cin >> a >> b >> c >> d;
  int max = max4(a, b, c, d);

  cout << max << endl;
}
