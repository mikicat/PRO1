#include <iostream>
using namespace std;

void print_line(char c, string s, bool b) {
  cout << s << "('" << c << "') = ";
  if (b) cout << "true" << endl;
  else cout << "false" << endl;
}

bool is_letter(char c) {
  if ((c >= 'A' and c <= 'Z') or (c >= 'a' and c <= 'z')) return true;
  else return false;
}
bool is_digit(char c) {
  if (c >= '0' and c <= '9') return true;
  else return false;
}
bool is_uppercase(char c) {
  if (c >= 'A' and c <= 'Z') return true;
  else return false;
}
bool is_vowel(char c) {
  if (not is_uppercase(c)) c = c - ('a' - 'A');
  if (c != 'A' and c != 'E' and c != 'I' and c != 'O' and c != 'U') return false;
  else return true;
}

int main() {
  char c;
  cin >> c;
  if (is_letter(c)) {
    print_line(c, "letter", true);
    print_line(c, "vowel", is_vowel(c));
    print_line(c, "consonant", not(is_vowel(c)));
    print_line(c, "uppercase", is_uppercase(c));
    print_line(c, "lowercase", not(is_uppercase(c)));
    print_line(c, "digit", false);
  }
  else {
    print_line(c, "letter", false);
    print_line(c, "vowel", false);
    print_line(c, "consonant", false);
    print_line(c, "uppercase", false);
    print_line(c, "lowercase", false);
    if (is_digit(c)) print_line(c, "digit", true);
    else print_line(c, "digit", false);
  }

}
