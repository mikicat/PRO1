#include <iostream>
using namespace std;

bool is_prime(int n) {
    bool prime = true;
    int i = 2;
    if (n < 2) prime = false;
    while(i*i <= n and prime) {
        if (n%i == 0) prime = false;
        ++i;
    }
    return prime;
}

int sum_of_digits(int n) {
    int digit = n%10;
    if (n/10 == 0) return n;
    else return digit+sum_of_digits(n/10);
}

bool is_perfect_prime(int n) {
    if (not is_prime(n)) return false;
    if (n/10 == 0) return true;
    else return is_perfect_prime(sum_of_digits(n));
}

int main() {
    int n;
    cin >> n;
    cout << is_perfect_prime(n) << endl;
    
}
