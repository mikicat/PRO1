#include <iostream>
using namespace std;

void digit_maxim_i_minim(int n, int& maxim, int& minim) {
  int d = n%10;
  if (d > maxim) maxim = d;
  else if (d < minim) minim = d;
  if (n/10 != 0) digit_maxim_i_minim(n/10, maxim, minim);
}
