#include <iostream>
using namespace std;


void bin(int n) {
    int b;
    b = n/2;
    if (b != 0) bin(b);
    cout << n-2*b;
}

void octal(int n) {
    int o;
    o = n/8;
    if (o != 0) octal(o);
    cout << n-8*o;
}

void hex(int n) {
    int h;
    char c;
    h = n/16;
    if (h != 0) hex(h);
    
    if ((n-16*h) >= 10) {
        c = 'A'+(n%16)-10;
        cout << c;
    }
    else cout << n-16*h;
}

int main() {
    int n;
    while (cin >> n) {
        cout << n << " = ";
        bin(n);
        cout << ", ";
        octal(n); 
        cout << ", ";
        hex(n);
        cout << endl;
    }
}
