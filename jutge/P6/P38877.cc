#include <iostream>
using namespace std;

int reverse(int i) {
  string w;
  if (cin >> w) {
    i = reverse(i);
    if (i >= 0) {
      cout << w << endl;
    }
  }
  return --i;

}

int main() {
  int n;
  cin >> n;
  reverse(n);
}
