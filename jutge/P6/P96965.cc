#include <iostream>
using namespace std;

int sum_of_digits(int x) {
    int n = 0;
    while (x != 0) {
        n += x%10;
        x /= 10;
    }
    return n;
}

int reduction_of_digits(int x) {
    int sum = sum_of_digits(x);
    if (sum/10 == 0) return sum;
    else return reduction_of_digits(sum);
}

int main() {
    cout << reduction_of_digits(33) << endl;
}
