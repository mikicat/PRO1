// NO ACABAT
#include <iostream>
using namespace std;

void bars(int n) {
  if (n == 1) cout << '*' << endl;
  else if (n == 2) {
    bars(n-1);
    for (int i = 1; i <= n; ++i) {
      for (int j = 0; j < i; ++j) cout << '*';
      cout << endl;
    }
  }
  else {
    for (int i = 0; i < n; ++i) bars(i);
    for (int i = 1; i <= n; ++i) {
      for (int j = 0; j < i; ++j) cout << '*';
      cout << endl;
    }
  }
}

int main() {
  int n;
  cin >> n;
  bars(n);
}
