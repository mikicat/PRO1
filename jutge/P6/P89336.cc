#include <iostream>
using namespace std;

void word(int& i) {
  string w;
  while (cin >> w) {
    ++i;
    int j = i;
    word(i);
    if (j <= i/2) cout << w << endl;
  }
}

int main() {
  int i = 0;
  word(i);
}
