#include <iostream>
using namespace std;



bool is_increasing(int n) {
    if (n/100 == 0) {
        if ((n%100)/10 > (n%10)) return false;
        else return true;
    }
    if ((n%100)/10 > (n%10)) return false;
    else return is_increasing(n/10);
}

int main() {
    int n;
    while (cin >> n) {
        cout << is_increasing(n) << endl;
    }
}
