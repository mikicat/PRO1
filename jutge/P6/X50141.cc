#include <iostream>
using namespace std;



int fatten(int x) {
    if (x < 10) return x;
    else {
        int r = fatten(x/10);
        if (x%10 > r%10) return r*10 + x%10;
        else return r*10 + r%10;
    }
}

int main() {
    int n;
    cin >> n;
    cout << fatten(n) << endl;
}
