#include <iostream>
using namespace std;

void word(string w) {
  if (cin >> w and w != "end") {
    word(w);
    cout << w << endl;
  }
}

int main() {
  string w;
  word(w);
}
