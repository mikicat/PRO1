#include <iostream>
#include <vector>
using namespace std;

int main() {
    int n;
    cin >> n;
    vector<string> line(n);
    
    for (int i = 0; i < n; ++i) cin >> line[i];
    
    for (int i = n-1; i >= 0; --i) {
        for (int j = line[i].length()-1; j >= 0; --j) cout << line[i][j];
        cout << endl;
    }
}
