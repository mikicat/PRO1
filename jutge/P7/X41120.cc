#include <iostream>
#include <vector>
using namespace std;

vector<int> obtain_peaks(const vector<int>& v) {
  vector<int> peaks(v.size());
  int p = 0;

  for (int i = 1; i < v.size()-1; ++i) {
    if (v[i - 1] < v[i] and v[i] > v[i + 1]) {
      peaks[p] = v[i];
      ++p;
    }
  }
  return peaks;
}

int main() {
  int n;
  cin >> n;

  vector<int> mountains(n);
  for (int i = 0; i < n; ++i) cin >> mountains[i];
  vector<int> peaks = obtain_peaks(mountains);

  cout << peaks.size() << ": ";
  for (int i = 0; i < peaks.size(); ++i) {
    cout << peaks[i];
    if (i != peaks.size() - 1) cout << ' ';
  }

  if (peaks.size() != 0) {
    int last = peaks[peaks.size() - 1];
    bool higher = false;

    for (int i = 0; i < peaks.size() - 1; ++i) {
      if (peaks[i] > last) {
        higher = true;
        cout << peaks[i];
      }
    }
  }
}
