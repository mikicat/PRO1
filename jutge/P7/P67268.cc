#include <iostream>
#include <vector>
using namespace std;

int main() {
    int n;
    while (cin >> n) {
        vector<int> seq(n);
        for (int i = 0; i < n; ++i) cin >> seq[i];
        
        for (int i = n-1; i > 0; --i) cout << seq[i] << ' ';
        if (n > 0) cout << seq[0];
        cout << endl;
    }
}
