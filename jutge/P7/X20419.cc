#include <iostream>
#include <vector>
using namespace std;

char most_frequent_letter(const string& s) {
  const int LENGTH_ALPHABET = 'z' - 'a' + 1;
  vector<int> frequency(LENGTH_ALPHABET, 0);
  int index = 0, max = 0;
  for (int i = 0; i < s.length(); ++i) ++frequency[s[i] - 'a'];
  for (int i = 0; i < LENGTH_ALPHABET; ++i) {
    if (frequency[i] > max) {
      max = frequency[i];
      index = i;
    }
  }
  return 'a' + index;
}

int main() {
  cout.setf(ios::fixed);
  cout.precision(2);
  int n;
  cin >> n;
  vector<string> list(n);
  double av = 0;
  for (int i = 0; i < n; ++i) {
    cin >> list[i];
    av += list[i].length();
  }
  av /= n;
  cout << av << endl;

  for (int i = 0; i < n; ++i) {
    if (list[i].length() >= av) cout << list[i] << ": " << most_frequent_letter(list[i]) << endl;
  }

}
