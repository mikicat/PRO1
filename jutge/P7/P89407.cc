#include <iostream>
#include <vector>
using namespace std;

string base_change(const int b, const int& n) {
  string res = "";
  for (int i = n; i != 0; i /= b) {
    if (i%b >= 10) {
      res += 'A' + i%b - 10;
    }
    else res += char(i%b + '0');
  }
  string changed;
  for (int i = res.length()-1; i >= 0; --i) changed += res[i];
  return changed;
}

bool compare(int b, const string& combined, const string& res) {
  if (combined.length() != res.length()) return false;
  vector<int> digits_c(b, 0);
  vector<int> digits_r(b, 0);
  for (int i = 0; i < res.length(); ++i) {
    if (combined[i] >= 'A') ++digits_c[int(combined[i] - 'A') + 10];
    else ++digits_c[int(combined[i] - '0')];

    if (res[i] >= 'A') ++digits_r[int(res[i] - 'A') + 10];
    else ++digits_r[int(res[i] - '0')];
  }

  for (int i = 0; i < b; ++i) {
    if (digits_c[i] != digits_r[i]) return false;
  }
  return true;
}

int main() {
  int x, y;
  while (cin >> x >> y) {
    cout << "solutions for " << x << " and " << y << endl;
    bool stable = false;
    for (int i = 2; i <= 16; ++i) {
      string x_b, y_b;
      x_b = base_change(i, x);
      y_b = base_change(i, y);
      string combined = x_b + y_b;
      string res = base_change(i, x*y);


      if (compare(i, combined, res)) {
        stable = true;
        cout << x_b << " * " << y_b << " = " << res << " (base " << i << ')' << endl;
      }
    }
    if (not(stable)) cout << "none of them" << endl;
    cout << endl;
  }
}
