#include <iostream>
#include <vector>
using namespace std;

int evaluate(const vector<int>& p, int x) {
    int total = 0;
    
    for (int i = p.size()-1; i>= 0; --i) total = total*x + p[i];
    return total;
}

int main() {
}
