#include <iostream>
#include <vector>
using namespace std;


bool is_prime(int n) {
    if (n < 2) return false;
    for (int i = 2; i*i < n; ++i) {
        if (n%i == 0) return false;
    }
    return true;
}

bool process(const vector<int>& list) {
    for (int i = 0; i < list.size(); ++i) {
        for (int j = 0; j < list.size(); ++j) {
            if (i != j) {
                if (list[i] == 1 and list[j] == 1) return true;
                if (list[i]%2 == 0 or list[j]%2 == 0) {
                    if (is_prime(list[i] + list[j])) return true;
                }
            }            
        }        
    }
    return false;
}

int main() {
    int n;
    while (cin >> n) {
        vector<int> list(n);
        for (int i = 0; i < n; ++i) cin >> list[i];
        
        if (process(list)) cout << "yes" << endl;
        else cout << "no" << endl;
    }
}
