#include <iostream>
#include <vector>
using namespace std;

typedef vector<int> cacatua;

int main() {
    int n;
    while (cin >> n) {
        cacatua elements(n);
        int total = 0;
        for (int i = 0; i < n; ++i) {
            cin >> elements[i];
            total += elements[i];
        }
        
        bool found = false;
        int i = 0;
        while (i < n and not(found)) {
            if (total - elements[i] == elements[i]) found = true;
            ++i;
        }
        if (found) cout << "YES" << endl;
        else cout << "NO" << endl;
    }
}
