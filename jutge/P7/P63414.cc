#include <iostream>
#include <vector>
using namespace std;

int main() {
    int n;
    cin >> n;
    vector<int> elements(1001, 0);
    for (int i = 0; i < n; ++i) {
        int el;
        cin >> el;
        ++elements[el-1000000000];
    }
    for (int i = 0; i < elements.size(); ++i) {
        if (elements[i] != 0) cout << 1000000000 + i << " : " << elements[i] << endl;
    }
    
}
