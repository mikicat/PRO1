#include <iostream>
#include <vector>
using namespace std;

int main() {
    int n;
    cin >> n;
    vector<int> numbers(n);
    for (int i = 0; i < n; ++i) cin >> numbers[i];
    
    int total = 0;
    for (int i = 0; i < n-1; ++i) {
        if (numbers[i] == numbers[n-1]) ++total;
    }
    cout << total << endl;
}
