#include <iostream>
using namespace std;
int pow(int b, int e);
int digit(int n, int pos);
int countDigits(int n);

int main() {
  int n;
  cin >> n;
  int sum = 0;
  int i = 1;
  while(i < countDigits(n))
  {
    sum += digit(n, i);
    i = i+2;
  }

  if (sum % 2 == 0) cout << n << " IS COOL" << endl;
  else cout << n << " IS NOT COOL" << endl;
}

int countDigits (int n) {
  int d = 1;
  while (n != 0) {
    ++d;
    n = n / 10;
  }
  return d;
}
int pow(int b, int e) {
  int n = 1;
  for (int i = 0; i < e; i++) {
    n *= b;
  }
  return n;
}

int digit(int n, int pos) {
  return ((n / pow(10, pos-1)) - (n / pow(10, pos) * 10));
}
