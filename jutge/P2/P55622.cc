#include <iostream>
using namespace std;

int main() {
  int x;
  cin >> x;
  int digits = 1;
  for(int i=x/10; i!=0; i=i/10)
  {
    digits++;
  }
  cout << "The number of digits of " << x << " is " << digits << "." << endl;
}
