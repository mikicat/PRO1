#include <iostream>
using namespace std;

void count(int s, int e);

int main() {
  int x, y;
  cin >> x >> y;
  if (x >= y) count(x, y);
  else count(y, x);

}

void count(int s, int e) {
  while(s >= e) {
    cout << s << endl;
    s--;
  }
}
