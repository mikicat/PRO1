#include <iostream>
using namespace std;

int pow(int b, int e);
int getDigits(int n);

int main() {
  int x, y;
  cin >> x;
  int end = getDigits(x);
  int total = 0;
  cout << "nombres que acaben igual que " << x << ":" << endl;
  while(cin >> y) {
    if (getDigits(y) == end) {
      cout << y << endl;
      ++total;
    }
  }
  cout << "total: " << total << endl;

}
int pow(int b, int e) {
  int res = 1;
  for(int i=0; i<e; ++i) {
    res *= b;
  }
  return res;
}

int getDigits(int n) {
  return (n - n / pow(10, 3) * 1000);
}
