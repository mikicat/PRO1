#include <iostream>
#include <cmath>
using namespace std;

int main() {
  int times;
  cin >> times;
  for (int i=0; i<times; ++i) {
    string type;
    double a;
    cin >> type;
    if (type == "rectangle") {
      double l, w;
      cin >> l >> w;
      a = l * w;
    }
    else {
      double r;
      cin >> r;
      a = M_PI * r * r;
    }
    cout.precision(6);
    cout << fixed << a << endl;
  }
}
