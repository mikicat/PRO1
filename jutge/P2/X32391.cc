#include <iostream>
using namespace std;

int main() {
  int d, n, t, a;
  cin >> d >> n >> t;
  int weeks = 0;
  for (int i=0; i < t; ++i) {
    cin >> a;
    if (a - d + n > 0) ++weeks;
    n = a - d + n;
  }
  cout << weeks << endl;
}
