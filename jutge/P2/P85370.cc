#include <iostream>
using namespace std;

int main() {
  double c, i, t;
  string m;
  cout.setf(ios::fixed);
  cout.precision(4);

  cin >> c >> i >> t >> m;

  if (m == "compound") {
    for (int j = 0; j < t; ++j) {
      c += c * (i / 100);
    }
  } else {
    c += c * (t * (i / 100));
  }
  cout << c << endl;
  cout << m[0];

}
