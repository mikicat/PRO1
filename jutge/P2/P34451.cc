#include <iostream>
using namespace std;

int main() {
  int n, m;
  cin >> n;
  int total = 0;
  while (cin >> m) {
    if (m % n == 0) ++total;
  }
  cout << total << endl;
}
