#include <iostream>
using namespace std;

int main() {
  int x = 0, y = 0;
  char mov;
  while(cin >> mov) {
    if (mov == 'n') y--;
    else if (mov == 's') y++;
    else if (mov == 'e') x++;
    else if (mov == 'w') x--;
  }
  cout << '(' << x << ", " << y << ')' << endl;
}
