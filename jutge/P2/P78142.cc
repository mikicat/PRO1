#include <iostream>
using namespace std;

int main() {
  int i = 0;
  double x, av = 0;
  while (cin >> x)
  {
    ++i;
    av += x;
  }
  cout.precision(2);
  cout << fixed << av / i << endl;
}
