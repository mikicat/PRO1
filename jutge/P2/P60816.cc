#include <iostream>
using namespace std;

int main() {
  int n;
  cin >> n;
  if (n == 0) cout << 0;
  while (n != 0) {
    int rev = n % 16;
    if (rev < 10) cout << rev;
    else cout << char(rev + 55);
    n /= 16;
  }
  cout << endl;
}
