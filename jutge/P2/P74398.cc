#include <iostream>
using namespace std;

int main(){
  int n;
  cin >> n;
  for (int i = 2; i <= 16; ++i) {
    int xifra = 0;
    int count = n;
    if (count == 0) xifra++;
    while (count != 0) {
      ++xifra;
      count /= i;
    }
    cout << "Base " << i << ": " << xifra << " cifras." << endl;
  }
}
