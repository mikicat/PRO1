// Multiplying by adding; product algorithm
#include <iostream>
using namespace std;
void mult_by_add();


int main()
{
  int x, y;
  int p = 0;
  cin >> x >> y;

  while (y > 0) {
    if (y % 2 == 0)
    {
      x = x*2;
      y = y/2;
    }
    else {
      p += x;
      y--;
    }
  }
  cout << p << endl;

  return 0;
}


void mult_by_add() {
  int x, y;
  int p = 0;
  cin >> x >> y;

  while (y > 0)
  {
    p += x;
    y--;
  }
  cout << p << endl;
}
