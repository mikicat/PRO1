#include <iostream>
using namespace std;

struct Subject {
  string name;
  int grade;
};

int main() {
  Subject subj;
  cin >> subj.name;
  int av = 0, i = 0;
  while (subj.name != ".") {
    ++i;
    cout << "I: " << i << endl;
    cin >> subj.grade;
    av += subj.grade;

    cin >> subj.name;
  }
  av /= i;
  cout << av << endl;
}
