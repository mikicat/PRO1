// Digit counter
#include <iostream>
using namespace std;

int main(){
  int c = 1;
  int n;
  cin >> n;
  while (n/10 != 0)
  {
    c++;
    n = n / 10;
  }
  cout << c << " digits" << endl;


  return 0;
}
