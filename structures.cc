// Apunts sobre la marxa, no compilen
#include <iostream>
typedef string Email;

typedef struct {
  string first_name, last_name;
  int age;
  bool can_vote
  Email email;
} Person;
///////////////////////////////////////

// Other way :
struct Address {
  string street;
  int number;
};

struct Person {
  string first_name, last_name;
  int age;
  Address address;
  bool can_vote;
};

Person p;
cin >> p.first_name >> p.last_name;
cin >> p.address.street;
cin >> p.address.number;
cin >> p.age;
p.can_vote = (p.age>= 18);


// Structures to return results
struct Result {
  int location;
  bool found;
};

Result search(int x, const vector<int>& A);

// Invariants in data structures
struct Rational {
  int num, den;
  // Inv; den > 0
};

struct Clock {
  inv hours, minutes, seconds;
  /* Inv: 0 <= hours < 24,
          0 <= minutes < 60,
          0 <= seconds < 60 */
};

// Example: rational numbers
void reduce(Rational& r) {
  int m = gcd(abs(r.num), r.den);

  r.num /= m;
  r.den /= m;
}
Rational sum(Rational a, Rational b) {
  Rational c;
  c.num = a.num*b.den + b.num*a.den;
  c.den = a.den*b.den;
  // c.den > 0 since a.den > 0 and b.den > 0

  reduce(c); // simplifies the fraction
  return c;
}
