// Is prime

#include <iostream>
using namespace std;

bool isPrime(int n);
void printNum(int n);

int main()
{
  int x;
  cin >> x;
  if (isPrime(x)) {
    cout << x << " is prime" << endl;
  } else { cout << x << " is not prime" << endl; }
}

bool isPrime(int n) {
  bool prime = true;
  int i=2;
  /*while (i < n)
  {
    if (n % i == 0)
    {
      prime = false;
      break;
    }
    i++;
  }*/
  for (int i=2; i < n; i++) {
    if (n % i == 0) {
      prime = false;
      break;
    }
  }
  return prime;
}

void printNum(int n) {
  int i = 1;
  while (i <= n) {
    cout << i << endl;
    i++;
  }
}
