#include <iostream>
using namespace std;
struct Clock {
  int hours;
  int minutes;
  int seconds;
};

Clock increment(Clock c) {
  c.seconds += 1;
  c.minutes += c.seconds/60;
  c.seconds = c.seconds%60;
  c.hours  += c.minutes/60;
  c.minutes = c.minutes%60;
  if (c.hours >= 24) c.hours -= 24;
  return c;
}


int main() {
  Clock c;
  cin >> c.hours >> c.minutes >> c.seconds;
  c = increment(c);
  cout << c.hours << ':' << c.minutes << ':' << c.seconds << endl;
}
