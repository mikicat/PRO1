#include <iostream>
#include <vector>
using namespace std;

bool is_pangram() {
  vector<bool> alphabet(26, false);
  char c;
  while (cin >> c) {
    if (c >= 'A' and c <= 'Z') c = c + ('a' - 'A');
    if (c >= 'a' and c <= 'z') alphabet[c - 'a'] = true;
  }

  for (int i = 0; i < N; ++i) {
    if (not alphabet[i]) return false;
  }
  return true;
}
