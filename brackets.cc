#include <iostream>
#include <vector>

bool is_open_br(char c);
bool is_clos_br(char c);
char match(char c);

bool brackets() {
  vector<char> unclosed;
  char c;
  while (cin >> c) {
    if (is_open_br(c)) unclosed.push_back(c);
    else if (unclosed.size() == 0) return false;
    else if (match(c) != unclosed[unclosed.size() - 1]) return false;
    else unclosed.pop_back();
  }

  return unclosed.size() == 0;
}

int main() {

}
