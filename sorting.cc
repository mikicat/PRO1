#include <iostream>
#include <vector>

#include <algorithm> // Includes sorting algorithms


using namespace std;

void selection_sort(vector<int>& v) {
  for (int i = 0; i < v.size(); ++i) {
    int min = v[i], pos = i;
    for (int j = i; j < v.size(); ++j) {
      if (v[j] < min) {
        min = v[j], pos = j;
      }
    }
    if (pos != i) {
      v[pos] = v[i];
      v[i] = min;
    }
  }
}

void insertion_sort(vector<int>& v, int n) {
  if (n > 0) {
    insertion_sort(v, n-1);
    int x = v[n];
    int j = n-1;
    while (j >= 0 and v[j] > x) {
      v[j+1] = v[j];
      --j;
    }
    v[j+1] = x;
  }
}

void bubble_sort(vector<int>& v) {
  bool swapped = true;
  while (swapped) {
    swapped = false;
    for (int i = 0; i < v.size()-1; ++i) {
      if (v[i] > v[i+1]) {
        int x = v[i];
        v[i] = v[i+1];
        v[i+1] = x;
        swapped = true;
      }
    }
  }
}

void merge(vector<int>& v, int left, int mid, int right) {
  int n = right - left + 1;
  vector<int> aux(n);
  int i = left;
  int j = mid + 1;
  int k = 0;
  while(i <= mid and j <= right) {
    if (v[i] <= v[j]) {
      aux[k] = v[i];
      ++i;
    }
    else {
      aux[k] = v[j];
      ++j;
    }
    ++k;
  }
  while(i <= mid) {
    aux[k] = v[i];
    ++k;
    ++i;
  }
  while(j <= right) {
    aux[k] = v[j];
    ++k;
    ++j;
  }
  for(k = 0; k < n; ++k) v[left+k] = aux[k];
}

void merge_sort(vector<int>& v, int left, int right) {
  if (left < right) {
    int m = (left + right)/2;
    merge_sort(v, left, m);
    merge_sort(v, m + 1, right);
    merge(v, left, m, right);
  }
}

int main() {
  vector<int> v(10);
  for (int i = 0; i < 10; ++i) cin >> v[i];
  merge_sort(v, 0, 10);
  for (int i = 0; i < 10; ++i) cout << v[i];
  cout << endl;

}
